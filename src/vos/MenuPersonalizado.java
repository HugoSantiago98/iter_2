package vos;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.*;

public class MenuPersonalizado extends Menu{

	@JsonProperty(value="fecha")
	private String fecha;

	public MenuPersonalizado(
			@JsonProperty(value="id_plato")Long idPlato, 
			@JsonProperty(value="nombre")String nombre, 
			@JsonProperty(value="stock")Integer stock, 
			@JsonProperty(value="descripcion")String descripcion,
			@JsonProperty(value="descripcionEN")String descripcionEN, 
			@JsonProperty(value="precioVenta")Double precioVenta,
			@JsonProperty(value="tiposComida")String tiposComida, 
			@JsonProperty(value="vendidos")Integer vendidos,
			@JsonProperty(value="restaurante")Long restaurante, 
			@JsonProperty(value="fechaInicio")String fechaInicio, 
			@JsonProperty(value="fechaFin")String fechaFin,
			@JsonProperty(value="platoFuerte")Long platoFuerte,
			@JsonProperty(value="platoBebida")Long platoBebida,
			@JsonProperty(value="platoPostre")Long platoPostre,
			@JsonProperty(value="platoAcom")Long platoAcom,
			@JsonProperty(value="platoEntrada")Long platoEntrada,
			@JsonProperty(value="fecha")String fecha) {
		
		super(idPlato, nombre, stock, descripcion, descripcionEN, precioVenta, tiposComida, vendidos, restaurante, fechaInicio, 
				fechaFin, platoFuerte, platoBebida, platoPostre, platoAcom, platoEntrada);
		this.fecha = fecha;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

		
	
}
