package vos;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.*;

public class Plato extends Producto{

	@JsonProperty(value="tiempoPreparacion")
	private Double tiempoPreparacion;
	
	@JsonProperty(value="costoPreparacion")
	private Double costoPreparacion;

	@JsonProperty(value="ingredientes")
	private Long ingredientes;
	
	@JsonProperty(value="tipoPlato")
	private TipoPlato tipoPlato;
	
	@JsonProperty(value="id_menu")
	private Long idMenu;
	
	

	public Plato(@JsonProperty(value="id_plato")Long idPlato, 
			@JsonProperty(value="nombre")String nombre, 
			@JsonProperty(value="stock")Integer stock, 
			@JsonProperty(value="descripcion")String descripcion,
			@JsonProperty(value="descripcionEN")String descripcionEN, 
			@JsonProperty(value="precioVenta")Double precioVenta,
			@JsonProperty(value="tiposComida")String tiposComida, 
			@JsonProperty(value="vendidos")Integer vendidos,
			@JsonProperty(value="restaurante")Long restaurante, 
			@JsonProperty(value="fechaInicio")String fechaInicio, 
			@JsonProperty(value="fechaFin")String fechaFin,
			
			@JsonProperty(value="tiempoPreparacion")Double tiempoPreparacion,
			@JsonProperty(value="costoPreparacion")Double costoPreparacion,
			@JsonProperty(value="ingredientes")Long ingredientes,
			@JsonProperty(value="tipoPlato")TipoPlato tipoPlato,
			@JsonProperty(value="id_menu")Long idMenu) {
		
		super(idPlato ,nombre, stock, descripcion, descripcionEN, precioVenta, tiposComida, vendidos,
				restaurante,fechaInicio,fechaFin);
		
		this.tiempoPreparacion = tiempoPreparacion;
		this.costoPreparacion = costoPreparacion;
		this.ingredientes = ingredientes;
		this.tipoPlato = tipoPlato;
		this.idMenu = idMenu;
	}

	
	public Long getIdMenu() {
		return idMenu;
	}


	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}


	public TipoPlato getTipoPlato() {
		return tipoPlato;
	}

	public void setTipoPlato(TipoPlato tipoPlato) {
		this.tipoPlato = tipoPlato;
	}

	public Double getTiempoPreparacion() {
		return tiempoPreparacion;
	}

	public void setTiempoPreparacion(Double tiempoPreparacion) {
		this.tiempoPreparacion = tiempoPreparacion;
	}

	public Double getCostoPreparacion() {
		return costoPreparacion;
	}

	public void setCostoPreparacion(Double costoPreparacion) {
		this.costoPreparacion = costoPreparacion;
	}


	public Long getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(Long ingredientes) {
		this.ingredientes = ingredientes;
	}
}
