package vos;
import java.util.Date;
import org.codehaus.jackson.annotate.*;

public class Reservacion {
	

	@JsonProperty(value="fecha")
	private Date fecha;
	
	@JsonProperty(value="comensales")
	private Integer comensales;
	
	@JsonProperty(value="usuarioCliente")
	private UsuarioCliente usuarioCliente;

	@JsonProperty(value="zona")
	private Zona zona;
	
	@JsonProperty(value="estadoReserva")
	private EstadoReserva estadoReserva;

	public Reservacion(EstadoReserva estadoReservaEnum, @JsonProperty(value="fecha")Date fecha, @JsonProperty(value="comensales")Integer comensales, 
			@JsonProperty(value="usuarioCliente")UsuarioCliente usuarioCliente,
			@JsonProperty(value="zona")Zona zona, @JsonProperty(value="estadoReserva")EstadoReserva estadoReserva) {
		
		
		this.fecha = fecha;
		this.comensales = comensales;
		this.usuarioCliente = usuarioCliente;
		this.zona = zona;
		this.estadoReserva = estadoReserva;
	}


	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getComensales() {
		return comensales;
	}

	public void setComensales(Integer comensales) {
		this.comensales = comensales;
	}

	public UsuarioCliente getUsuarioCliente() {
		return usuarioCliente;
	}

	public void setUsuarioCliente(UsuarioCliente usuarioCliente) {
		this.usuarioCliente = usuarioCliente;
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public EstadoReserva getEstadoReserva() {
		return estadoReserva;
	}

	public void setEstadoReserva(EstadoReserva estadoReserva) {
		this.estadoReserva = estadoReserva;
	}
}
