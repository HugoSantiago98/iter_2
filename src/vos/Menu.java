package vos;
import org.codehaus.jackson.annotate.*;

import java.util.Date;
import java.util.List;

public class Menu extends Producto{

	@JsonProperty(value="platoFuerte")
	private Long platoFuerte;
	
	@JsonProperty(value="platoBebida")
	private Long platoBebida;
	
	@JsonProperty(value="platoPostre")
	private Long platoPostre;
	
	@JsonProperty(value="platoAcom")
	private Long platoAcom;
	
	@JsonProperty(value="platoEntrada")
	private Long platoEntrada;
	
	
	public Menu(
			@JsonProperty(value="id_plato")Long idPlato, 
			@JsonProperty(value="nombre")String nombre, 
			@JsonProperty(value="stock")Integer stock, 
			@JsonProperty(value="descripcion")String descripcion,
			@JsonProperty(value="descripcionEN")String descripcionEN, 
			@JsonProperty(value="precioVenta")Double precioVenta,
			@JsonProperty(value="tiposComida")String tiposComida, 
			@JsonProperty(value="vendidos")Integer vendidos,
			@JsonProperty(value="restaurante")Long restaurante, 
			@JsonProperty(value="fechaInicio")String fechaInicio, 
			@JsonProperty(value="fechaFin")String fechaFin,
			@JsonProperty(value="platoFuerte")Long platoFuerte,
			@JsonProperty(value="platoBebida")Long platoBebida,
			@JsonProperty(value="platoPostre")Long platoPostre,
			@JsonProperty(value="platoAcom")Long platoAcom,
			@JsonProperty(value="platoEntrada")Long platoEntrada) {
		
		super(idPlato ,nombre, stock, descripcion, descripcionEN, precioVenta, tiposComida, vendidos,
				restaurante,fechaInicio,fechaFin);
		this.platoFuerte = platoFuerte;
		this.platoAcom = platoAcom;
		this.platoBebida = platoBebida;
		this.platoEntrada = platoEntrada;
		this.platoPostre = platoPostre;
		
	}


	public Long getPlatoFuerte() {
		return platoFuerte;
	}


	public void setPlatoFuerte(Long platoFuerte) {
		this.platoFuerte = platoFuerte;
	}


	public Long getPlatoBebida() {
		return platoBebida;
	}


	public void setPlatoBebida(Long platoBebida) {
		this.platoBebida = platoBebida;
	}


	public Long getPlatoPostre() {
		return platoPostre;
	}


	public void setPlatoPostre(Long platoPostre) {
		this.platoPostre = platoPostre;
	}


	public Long getPlatoAcom() {
		return platoAcom;
	}


	public void setPlatoAcom(Long platoAcom) {
		this.platoAcom = platoAcom;
	}


	public Long getPlatoEntrada() {
		return platoEntrada;
	}


	public void setPlatoEntrada(Long platoEntrada) {
		this.platoEntrada = platoEntrada;
	}
	
}