package vos;

public enum EstadoPedido {

	ORDENADO,
	CANCELADO,
	SERVIDO,
	CONFIRMADO;
}
