package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class ExitoRotondes {

	@JsonProperty (value= "nit_Restaurante")
	private long nit_Restaurante;
	
	@JsonProperty (value= "iD_Producto")
	private long iD_Producto;
	
	@JsonProperty (value= "productosVendidos")
	private long productosVendidos;
	
	@JsonProperty (value= "frecuenciaRestaurante")
	private long frecuenciaRestaurante;
	
	@JsonProperty (value= "nit_Restaurante_Menor")
	private long nit_Restaurante_Menor;
	
	@JsonProperty (value= "iD_Producto_Menor")
	private long iD_Producto_Menor;
	
	@JsonProperty (value= "productosMenosVendidos")
	private long productosMenosVendidos;
	
	@JsonProperty (value= "frecuenciaRestaurante_Menor")
	private long frecuenciaRestaurante_Menor;
	
	@JsonProperty (value= "d�a")
	private String d�a;
	
	public ExitoRotondes (
		@JsonProperty (value= "d�a") String d�a,
		@JsonProperty(value= "nit_Restaurante") long nit_Restaurante,
		@JsonProperty(value= "iD_Producto") long iD_Producto,
		@JsonProperty (value= "productosVendidos") long productosVendidos,
		@JsonProperty (value= "frecuenciaRestaurante") long frecuenciaRestaurante,
		@JsonProperty (value= "nit_Restaurante_Menor") long nit_Restaurante_Menor,
		@JsonProperty (value= "iD_Producto_Menor") long iD_Producto_Menor,
		@JsonProperty (value= "productosMenosVendidos") long productosMenosVendidos,
		@JsonProperty (value= "frecuenciaRestaurante_Menor") long frecuenciaRestaurante_Menor) {
		
		this.d�a=d�a;
		this.nit_Restaurante=nit_Restaurante;
		this.iD_Producto=iD_Producto;
		this.productosVendidos=productosVendidos;
		this.frecuenciaRestaurante=frecuenciaRestaurante;
		this.nit_Restaurante_Menor=nit_Restaurante_Menor;
		this.iD_Producto_Menor=iD_Producto_Menor;
		this.productosMenosVendidos=productosMenosVendidos;
		this.frecuenciaRestaurante_Menor=frecuenciaRestaurante_Menor;
		
	}

	public long getNit_Restaurante() {
		return nit_Restaurante;
	}

	public void setNit_Restaurante(long nit_Restaurante) {
		this.nit_Restaurante = nit_Restaurante;
	}

	public long getiD_Producto() {
		return iD_Producto;
	}

	public void setiD_Producto(long iD_Producto) {
		this.iD_Producto = iD_Producto;
	}

	public long getProductosVendidos() {
		return productosVendidos;
	}

	public void setProductosVendidos(long productosVendidos) {
		this.productosVendidos = productosVendidos;
	}

	public long getFrecuenciaRestaurante() {
		return frecuenciaRestaurante;
	}

	public void setFrecuenciaRestaurante(long frecuenciaRestaurante) {
		this.frecuenciaRestaurante = frecuenciaRestaurante;
	}

	public long getNit_Restaurante_Menor() {
		return nit_Restaurante_Menor;
	}

	public void setNit_Restaurante_Menor(long nit_Restaurante_Menor) {
		this.nit_Restaurante_Menor = nit_Restaurante_Menor;
	}

	public long getiD_Producto_Menor() {
		return iD_Producto_Menor;
	}

	public void setiD_Producto_Menor(long iD_Producto_Menor) {
		this.iD_Producto_Menor = iD_Producto_Menor;
	}

	public long getProductosMenosVendidos() {
		return productosMenosVendidos;
	}

	public void setProductosMenosVendidos(long productosMenosVendidos) {
		this.productosMenosVendidos = productosMenosVendidos;
	}

	public long getFrecuenciaRestaurante_Menor() {
		return frecuenciaRestaurante_Menor;
	}

	public void setFrecuenciaRestaurante_Menor(long frecuenciaRestaurante_Menor) {
		this.frecuenciaRestaurante_Menor = frecuenciaRestaurante_Menor;
	}

	public String getD�a() {
		return d�a;
	}

	public void setD�a(String d�a) {
		this.d�a = d�a;
	}
}
