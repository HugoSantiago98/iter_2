package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class InfoVO {
	
	@JsonProperty (value= "nit")
	private long nit;
	
	@JsonProperty (value= "cantidad")
	private long cantidad;
	
	@JsonProperty (value= "precio")
	private long precio;
	
	public InfoVO(@JsonProperty (value= "nit") long nit, @JsonProperty (value= "cantidad") long cantidad, @JsonProperty (value= "precio") long precio)
	{
		this.nit = nit;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public long getNit() {
		return nit;
	}

	public void setNit(long nit) {
		this.nit = nit;
	}

	public long getCantidad() {
		return cantidad;
	}

	public void setCantidad(long cantidad) {
		this.cantidad = cantidad;
	}

	public long getPrecio() {
		return precio;
	}

	public void setPrecio(long precio) {
		this.precio = precio;
	}
	
	
	
}
