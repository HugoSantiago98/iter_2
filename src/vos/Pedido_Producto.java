package vos;
import org.codehaus.jackson.annotate.*;

public class Pedido_Producto extends Pedido{

	@JsonProperty(value="cantidadUniandes")
	private Long cantidadUniandes;
	
	@JsonProperty(value="pedido")
	private Long pedido;

	@JsonProperty(value="producto")
	private Long producto;
	
	@JsonProperty(value="restaurante")
	private Long nit_restaurante;
	
	@JsonProperty(value="equivalente")
	private Long equivalente;


	public Pedido_Producto(
			@JsonProperty(value="fecha")String fecha, 
			@JsonProperty(value="mesa")long mesa, 
			@JsonProperty(value="estadoPedido")long estadoPedido,
			
			@JsonProperty(value="cantidadUniandes") long cantidadUnidades,
			@JsonProperty(value="pedido")long pedido,
			@JsonProperty(value="producto")long producto,
			@JsonProperty(value="restaurante")long nit_restaurante,
			@JsonProperty(value="equivalente")long equivalente) {
		super(fecha, mesa, estadoPedido);
		
		this.cantidadUniandes = cantidadUnidades;
		this.pedido = pedido;
		this.producto = producto;
		this.nit_restaurante = nit_restaurante;
		this.equivalente = equivalente;
	}

	
	public Long getEquivalente() {
		return equivalente;
	}


	public void setEquivalente(Long equivalente) {
		this.equivalente = equivalente;
	}


	public Long getNit_restaurante() {
		return nit_restaurante;
	}


	public void setNit_restaurante(Long nit_restaurante) {
		this.nit_restaurante = nit_restaurante;
	}


	public Long getCantidadUniandes() {
		return cantidadUniandes;
	}

	public void setCantidadUniandes(Long cantidadUniandes) {
		this.cantidadUniandes = cantidadUniandes;
	}

	public Long getPedido() {
		return pedido;
	}

	public void setPedido(Long pedido) {
		this.pedido = pedido;
	}

	public Long getProducto() {
		return producto;
	}

	public void setProducto(Long producto) {
		this.producto = producto;
	}
}
