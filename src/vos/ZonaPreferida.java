package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class ZonaPreferida {
	
	/**
	 * atributo que representa la llave foranea de el usuari0
	 */
	@JsonProperty(value="usuario")
	private long usuario;
	
	/**
	 * atributo que representa el nombre de la zona preferida
	 */
	@JsonProperty(value="zona")
	private String zona;
	
	@JsonProperty(value ="id")
	private long id;
	
	/**
	 * constructor de una zona preferida a travez de un JSON
	 * @param id llave foranea de un usuario
	 * @param zona nombre, usada como llave foranea, de una zona
	 */
	public ZonaPreferida(@JsonProperty(value="usuario")long idu , @JsonProperty(value="zona")String zona, @JsonProperty(value ="id") long id )
	{
		this.usuario = idu;
		this.zona = zona;
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getZona() {
		return zona;
	}
	
	/**
	 * 
	 * @param zona
	 */
	public void setZona(String zona) {
		this.zona = zona;
	}

	public long getIdu() {
		return usuario;
	}

	public void setIdu(long idu) {
		this.usuario = idu;
	}
	
	
}
