package vos;

import org.codehaus.jackson.annotate.*;

public class TipoComida {

	@JsonProperty(value = "id")
	private long id;
	
	@JsonProperty( value = "tipo")
	private String tipo;

	public TipoComida (@JsonProperty(value = "id") long id , @JsonProperty(value = "tipo") String tipo)
	{
		this.id = id;
		this.tipo = tipo;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
