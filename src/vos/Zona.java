package vos;
import org.codehaus.jackson.annotate.*;

public class Zona {
	
	@JsonProperty(value="nombreZona")
	private String nombreZona;

	@JsonProperty(value="abierto")
	private boolean abierto;

	@JsonProperty(value="capacidad")
	private Integer capacidad;

	@JsonProperty(value="discapacitados")
	private boolean discapacitados;

	@JsonProperty(value="descripcionTecnica")
	private String descripcionTecnica;

	public Zona(@JsonProperty(value="nombreZona")String nombreZona, @JsonProperty(value="abierto")boolean abierto, 
			@JsonProperty(value="capacidad")Integer capacidad, @JsonProperty(value="discapacitados")boolean discapacitados,
			@JsonProperty(value="descripcionTecnica")String descripcionTecnica) {
	
		this.nombreZona = nombreZona;
		this.abierto = abierto;
		this.capacidad = capacidad;
		this.discapacitados = discapacitados;
		this.descripcionTecnica = descripcionTecnica;
	}


	public String getNombreZona() {
		return nombreZona;
	}

	public void setNombreZona(String nombreZona) {
		this.nombreZona = nombreZona;
	}

	public boolean isAbierto() {
		return abierto;
	}

	public void setAbierto(boolean abierto) {
		this.abierto = abierto;
	}

	public Integer getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	public boolean isDiscapacitados() {
		return discapacitados;
	}

	public void setDiscapacitados(boolean discapacitados) {
		this.discapacitados = discapacitados;
	}

	public String getDescripcionTecnica() {
		return descripcionTecnica;
	}

	public void setDescripcionTecnica(String descripcionTecnica) {
		this.descripcionTecnica = descripcionTecnica;
	}
	
	
}
