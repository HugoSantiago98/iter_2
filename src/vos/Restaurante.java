package vos;
import java.util.List;

import org.codehaus.jackson.annotate.*;

public class Restaurante {

	@JsonProperty(value="NIT")
	private long id;
	
	@JsonProperty(value = "nombre")
	private String nombre;
	
	@JsonProperty(value="tipocomida")
	private long tipoComida;
	
	@JsonProperty(value="paginaweb")
	private String paginaWeb;
	
	@JsonProperty(value="cuenta")
	private String cuenta;
	
	@JsonProperty(value="idencargado")
	private long idencargado;
	
	public Restaurante(@JsonProperty(value="nombre")String nombre, @JsonProperty(value="tipocomida")long tipoComida, 
			@JsonProperty(value="paginaweb")String paginaWeb, @JsonProperty(value="cuenta")String cuentaDePago,
			@JsonProperty(value="idencargado")long usuariRestaurante)
			 {
		this.nombre = nombre;
		this.tipoComida = tipoComida;
		this.paginaWeb = paginaWeb;
		this.cuenta = cuentaDePago;
		this.idencargado = usuariRestaurante;
	}

	

	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getTipoComida() {
		return tipoComida;
	}

	public void setTipoComida(long tipoComida) {
		this.tipoComida = tipoComida;
	}

	public String getPaginaWeb() {
		return paginaWeb;
	}

	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}

	public String getCuentaDePago() {
		return cuenta;
	}

	public void setCuentaDePago(String cuentaDePago) {
		this.cuenta = cuentaDePago;
	}

	public long getUsuariRestaurante() {
		return idencargado;
	}

	public void setUsuariRestaurante(long usuariRestaurante) {
		this.idencargado = usuariRestaurante;
	}

}
