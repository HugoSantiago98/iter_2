package vos;
import org.codehaus.jackson.annotate.*;

public class UsuarioGeneral {

	//Atributos
	/**
	 * Usuario
	 */
	@JsonProperty(value="usuario")
	private String usuario;
	
	/**
	 * Contraseña
	 */
	@JsonProperty(value="contraseña")
	private String contraseña;
	
	/**
	 * NOmbre
	 */
	@JsonProperty(value="nombre")
	private String nombre;
	
	/**
	 * email
	 */
	@JsonProperty(value="email")
	private String email;
	
	/**
	 * Identificacion
	 */
	@JsonProperty(value="identificacion")
	private String identificacion;
	
	@JsonProperty(value="rolUsuario")
	RolUsuario rolUsuario;
	
	
	public RolUsuario getRolUsuario() {
		return rolUsuario;
	}
	public void setRolUsuario(RolUsuario rolUsuario) {
		this.rolUsuario = rolUsuario;
	}
	/**
	 * 
	 * @param usuario
	 * @param contraseña
	 * @param nombre
	 * @param email
	 * @param identifcacion
	 */
	public UsuarioGeneral(@JsonProperty(value="usuario")String usuario, @JsonProperty(value="contraseña")String contraseña, 
			@JsonProperty(value="nombre")String nombre, @JsonProperty(value="email")String email, 
			@JsonProperty(value="identificacion")String identificacion, @JsonProperty(value="rolUsuario")RolUsuario rolUsuario) {

		this.usuario = usuario;
		this.contraseña = contraseña;
		this.nombre = nombre;
		this.email = email;
		this.identificacion = identificacion;
		this.rolUsuario = rolUsuario;
	}
	/**
	 * Da usuario
	 * @return
	 */
	public String getUsuario() {
		return usuario;
	}
	/**
	 * set usuario
	 * @param usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	/**
	 * Da la contraseña
	 * @return
	 */
	public String getContraseña() {
		return contraseña;
	}
	/**
	 * Modifica la contraseña
	 * @param contraseña
	 */
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	/**
	 * Da el nombre
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Mofica el nombre
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * Da el email
	 * @return
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * mofica el email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Da la identificacion
	 * @return
	 */
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentifcacion(String identificacion) {
		this.identificacion = identificacion;
	}
	
	
}
