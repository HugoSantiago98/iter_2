package vos;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.*;

public class Producto {


	@JsonProperty(value="id_plato")
	private Long idPlato;

	@JsonProperty(value="nombre")
	private String nombre;

	@JsonProperty(value="stock")
	private Integer stock;

	@JsonProperty(value="descripcion")
	private String descripcion;

	@JsonProperty(value="descripcionEN")
	private String descripcionEN;

	@JsonProperty(value="precioVenta")
	private Double precioVenta;

	@JsonProperty(value="tiposComida")
	private String tiposComida;

	@JsonProperty(value="vendidos")
	private Integer vendidos;

	@JsonProperty(value="restaurante")
	private Long Nitrestaurante;

	@JsonProperty(value="fechaInicio")
	private String fechaInicio;

	@JsonProperty(value="fechaFin")
	private String fechaFin;


	public Producto(@JsonProperty(value="id_plato")Long idPlato,
			@JsonProperty(value="nombre")String nombre, 
			@JsonProperty(value="stock")Integer stock, 
			@JsonProperty(value="descripcion")String descripcion, 
			@JsonProperty(value="descripcionEN")String descripcionEN, 
			@JsonProperty(value="precioVenta")Double precioVenta,
			@JsonProperty(value="tiposComida")String tiposComida, 
			@JsonProperty(value="vendidos")Integer vendidos, 
			@JsonProperty(value="restaurante")Long Nitrestaurante,
			@JsonProperty(value="fechaInicio")String fechaInicio,
			@JsonProperty(value="fechaFin")String fechaFin) {

		this.idPlato = idPlato;
		this.nombre = nombre;
		this.stock = stock;
		this.descripcion = descripcion;
		this.descripcionEN = descripcionEN;
		this.precioVenta = precioVenta;
		this.tiposComida = tiposComida;
		this.vendidos = vendidos;
		this.Nitrestaurante = Nitrestaurante;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}

	public Long getIdPlato() {
		return idPlato;
	}
	public void setIdPlato(Long idPlato) {
		this.idPlato = idPlato;
	}
	

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionEN() {
		return descripcionEN;
	}

	public void setDescripcionEN(String descripcionEN) {
		this.descripcionEN = descripcionEN;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	

	public String getTiposComida() {
		return tiposComida;
	}

	public void setTiposComida(String tiposComida) {
		this.tiposComida = tiposComida;
	}

	public Integer getVendidos() {
		return vendidos;
	}

	public void setVendidos(Integer vendidos) {
		this.vendidos = vendidos;
	}

	public Long getNitrestaurante() {
		return Nitrestaurante;
	}

	public void setNitrestaurante(Long nitrestaurante) {
		Nitrestaurante = nitrestaurante;
	}

}
