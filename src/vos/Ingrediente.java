package vos;
import org.codehaus.jackson.annotate.*;

public class Ingrediente {
	
	@JsonProperty(value = "id")
	private long id;
	
	@JsonProperty(value="nombre")
	private String nombre;
	
	@JsonProperty(value="descripcion")
	private String descripcion;
	
	@JsonProperty(value="descripcionEN")
	private String descripcionEN;
	
	
	@JsonProperty(value="precio")
	private Double precio;
	
	@JsonProperty(value="stock")
	private Double stock;
	
	@JsonProperty(value="restaurante")
	private long restaurante;
	
	public Ingrediente(
			@JsonProperty(value = "id") long id ,
			@JsonProperty(value="nombre")String nombre, 	
			@JsonProperty(value="descripcion")String descripcion, 
			@JsonProperty(value="descripcionEN")String descripcionEN, 
			@JsonProperty(value="precio")Double precio,
			@JsonProperty(value="stock")Double stock, 
			@JsonProperty(value="nit")long restaurante) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.descripcionEN = descripcionEN;
		this.precio = precio;
		this.stock = stock;
		this.restaurante = restaurante;
	}

	
	
	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionEN() {
		return descripcionEN;
	}

	public void setDescripcionEN(String descripcionEN) {
		this.descripcionEN = descripcionEN;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Double getStock() {
		return stock;
	}

	public void setStock(Double stock) {
		this.stock = stock;
	}

	public long getRestaurante() {
		return restaurante;
	}

	public void setRestaurante(long restaurante) {
		this.restaurante = restaurante;
	}
}
