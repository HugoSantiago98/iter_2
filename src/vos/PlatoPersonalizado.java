package vos;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.*;

public class PlatoPersonalizado extends Plato{

	@JsonProperty(value="fecha")
	private Date fecha;
	
	@JsonProperty(value="adiciones")
	private Long adiciones;

	public PlatoPersonalizado(@JsonProperty(value="id_plato")Long idPlato, 
			@JsonProperty(value="nombre")String nombre, 
			@JsonProperty(value="stock")Integer stock, 
			@JsonProperty(value="descripcion")String descripcion,
			@JsonProperty(value="descripcionEN")String descripcionEN, 
			@JsonProperty(value="precioVenta")Double precioVenta,
			@JsonProperty(value="tiposComida")String tiposComida, 
			@JsonProperty(value="vendidos")Integer vendidos,
			@JsonProperty(value="restaurante")Long restaurante, 
			@JsonProperty(value="fechaInicio")String fechaInicio, 
			@JsonProperty(value="fechaFin")	String fechaFin,
			@JsonProperty(value="tiempoPreparacion")Double tiempoPreparacion,
			@JsonProperty(value="costoPreparacion")Double costoPreparacion,
			
			@JsonProperty(value="ingredientes")Long ingredientes,
			@JsonProperty(value="tipoPlato")TipoPlato tipoPlato,
			
			@JsonProperty(value="fecha")Date fecha, 
			@JsonProperty(value="adiciones")Long adiciones) {
		
		super(idPlato,nombre, stock, descripcion, descripcionEN, precioVenta, tiposComida, vendidos, restaurante, fechaInicio,
				fechaFin, tiempoPreparacion, costoPreparacion, ingredientes, tipoPlato, adiciones);
		this.fecha = fecha;
		this.adiciones = adiciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Long getAdiciones() {
		return adiciones;
	}

	public void setAdiciones(Long adiciones) {
		this.adiciones = adiciones;
	}

	
}
