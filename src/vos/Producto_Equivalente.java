package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class Producto_Equivalente {

	@JsonProperty(value="equivalente")
	private long equivalente;
	
	@JsonProperty(value="id")
	private long id;
	
	@JsonProperty(value="nit")
	private long nit;

	public Producto_Equivalente(
			@JsonProperty(value="equivalente")long equivalente, 
			@JsonProperty(value="id")long id, 
			@JsonProperty(value="nit")long nit) {
		
		
		this.equivalente = equivalente;
		this.id = id;
		this.nit = nit;
	}

	public long getEquivalente() {
		return equivalente;
	}

	public void setEquivalente(long equivalente) {
		this.equivalente = equivalente;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getNit() {
		return nit;
	}

	public void setNit(long nit) {
		this.nit = nit;
	}
	
	
}
