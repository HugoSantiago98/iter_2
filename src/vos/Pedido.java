package vos;
import java.util.Date;

import org.codehaus.jackson.annotate.*;

public class Pedido {

	@JsonProperty(value="fecha")
	private String fecha;
	
	@JsonProperty(value="mesa")
	private long mesa;

	
	@JsonProperty(value="estadoPedido")
	private long estadoPedido;

	public Pedido(@JsonProperty(value="fecha")String fecha, @JsonProperty(value="mesa")long mesa,
			@JsonProperty(value="estadoPedido")long estadoPedido) {
	
		this.fecha = fecha;
		this.mesa = mesa;
		this.estadoPedido = estadoPedido;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public long getMesa() {
		return mesa;
	}

	public void setMesa(long mesa) {
		this.mesa = mesa;
	}

	public long getEstadoPedido() {
		return estadoPedido;
	}

	public void setEstadoPedido(long estadoPedido) {
		this.estadoPedido = estadoPedido;
	}

	
}
