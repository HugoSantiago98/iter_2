package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class Ingrediente_Equivalente extends Ingrediente{

	@JsonProperty(value="equivalente")
	private long equivalente;
	
	public Ingrediente_Equivalente(@JsonProperty(value = "id") long id ,
			@JsonProperty(value="nombre")String nombre, 	
			@JsonProperty(value="descripcion")String descripcion, 
			@JsonProperty(value="descripcionEN")String descripcionEN, 
			@JsonProperty(value="precio")Double precio,
			@JsonProperty(value="stock")Double stock, 
			@JsonProperty(value="nit")long restaurante,
			
			@JsonProperty(value="equivalente")long equivalente) {
		super(id, nombre, descripcion, descripcionEN, precio, stock, restaurante);
		this.equivalente =equivalente;
	}

	public long getEquivalente() {
		return equivalente;
	}

	public void setEquivalente(long equivalente) {
		this.equivalente = equivalente;
	}

	
}
