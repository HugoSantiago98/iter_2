package vos;
import java.util.List;

import org.codehaus.jackson.annotate.*;

public class Mesa {

	//Atributos
	/**
	 * Identificador de la mesa
	 */
	@JsonProperty(value="identificadorMesa")
	private Integer identificadorMesa;
	
	/**
	 * Capacidad
	 */
	@JsonProperty(value="capacidad")
	private Integer capacidad;
	
	/**
	 *zona Asociada 
	 */
	@JsonProperty(value="capacidad")
	private Zona zona;
	
	/**
	 * Pedidos de la mesa
	 */
	@JsonProperty(value="pedidos")
	private List<Pedido> pedidos;

	/**
	 * 
	 * @param identificadorMesa
	 * @param capacidad
	 */
	
	
	public Mesa(@JsonProperty(value="identificadorMesa")Integer identificadorMesa, 
			@JsonProperty(value="capacidad")Integer capacidad,
			@JsonProperty(value="pedidos")List<Pedido> pedidos) {

		this.identificadorMesa = identificadorMesa;
		this.capacidad = capacidad;
		this.pedidos = pedidos;
	}

	/**
	 * Da el identificador
	 * @return
	 */
	public Integer getIdentificadorMesa() {
		return identificadorMesa;
	}
	/**
	 * Modifica el identificador
	 * @param identificadorMesa
	 */

	public void setIdentificadorMesa(Integer identificadorMesa) {
		this.identificadorMesa = identificadorMesa;
	}
	
	/**
	 * Da la capacidad de la mesa
	 * @return
	 */

	public Integer getCapacidad() {
		return capacidad;
	}
	
	/**
	 * Modifica la capacidad 
	 * @param capacidad
	 */

	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}
	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	
}
