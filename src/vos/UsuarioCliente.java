package vos;
import org.codehaus.jackson.annotate.*;

import java.util.List;

public class UsuarioCliente extends UsuarioGeneral {
	
	//Atributos
	/**
	 * Lista de preferencias por categoria
	 */
	@JsonProperty(value="preferenciaCategoria")
	private Long preferenciaCategoria; 
	/**
	 * Preferencia del precio
	 */
	@JsonProperty(value="preferenciaPrecio")
	private Double preferenciaPrecio;
	
	/**
	 * Reservaciones
	 */
	@JsonProperty(value="reservaciones")
	private Long reservaciones;
	
	/**
	 * Mesa
	 */
	@JsonProperty(value="mesa")
	private Long mesa;
	
	/**
	 * Zona preferida
	 */
	@JsonProperty(value="zona")
	private String zonas;
	
	/**
	 */
	public UsuarioCliente(@JsonProperty(value="usuario")String usuario,
			@JsonProperty(value="contraseņa")String contraseņa, 
			@JsonProperty(value="nombre")String nombre, 
			@JsonProperty(value="email")String email,
			@JsonProperty(value="identificacion")String identifcacion,
			@JsonProperty(value="rolUsuario")RolUsuario tipoUsuario, 
			
			@JsonProperty(value="preferenciaPrecio")Double preferenciaPrecio,
			@JsonProperty(value="preferenciaCategoria")Long preferenciaCategoria, 
			@JsonProperty(value="reservaciones")Long reservaciones,
			@JsonProperty(value="mesa")Long mesa,
			@JsonProperty(value="zona")String zonas) {
		
		super(usuario, contraseņa, nombre, email, identifcacion, tipoUsuario);
		this.preferenciaPrecio = preferenciaPrecio;
		this.preferenciaCategoria = preferenciaCategoria;
		this.reservaciones = reservaciones;
		this.mesa = mesa;
		this.zonas = zonas;
	}


	/**
	 * Da la lista de preferencias
	 * @return
	

	/**
	 * Da la preferencia del precio
	 * @return
	 */
	public Double getPreferenciaPrecio() {
		return preferenciaPrecio;
	}


	


	public Long getPreferenciaCategoria() {
		return preferenciaCategoria;
	}


	public void setPreferenciaCategoria(Long preferenciaCategoria) {
		this.preferenciaCategoria = preferenciaCategoria;
	}




	public String getZonas() {
		return zonas;
	}


	public void setZonas(String zonas) {
		this.zonas = zonas;
	}


	/**
	 * Mofica la preferencia del precio
	 * @param preferenciaPrecio
	 */
	public void setPreferenciaPrecio(Double preferenciaPrecio) {
		this.preferenciaPrecio = preferenciaPrecio;
	}


	public Long getReservaciones() {
		return reservaciones;
	}


	public void setReservaciones(Long reservaciones) {
		this.reservaciones = reservaciones;
	}


	public Long getMesa() {
		return mesa;
	}


	public void setMesa(Long mesa) {
		this.mesa = mesa;
	}

}
