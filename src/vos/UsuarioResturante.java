package vos;
import org.codehaus.jackson.annotate.*;

public class UsuarioResturante extends UsuarioGeneral{

	/**
	 * 
	 * @param usuario
	 * @param contraseņa
	 * @param nombre
	 * @param email
	 * @param identifcacion
	 * @param tipoUsuario
	 */
	@JsonProperty(value="restaurante")
	private Restaurante restaurante;

	public UsuarioResturante(String usuario, String contraseņa, String nombre, String email, String identifcacion,
			RolUsuario tipoUsuario,@JsonProperty(value="restaurante")Restaurante restaurante) {
		super(usuario, contraseņa, nombre, email, identifcacion, tipoUsuario);
		this.restaurante = restaurante;
	}

	public Restaurante getRestaurante() {
		return restaurante;
	}

	public void setRestaurante(Restaurante restaurante) {
		this.restaurante = restaurante;
	}

}
