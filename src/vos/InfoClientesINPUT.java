package vos;

import org.codehaus.jackson.annotate.JsonProperty;

public class InfoClientesINPUT {
	
	@JsonProperty("date1")
	private String date1;
	
	@JsonProperty ("date2")
	private String date2;
	
	
	//getters and setters
	
	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}
	
	
}
