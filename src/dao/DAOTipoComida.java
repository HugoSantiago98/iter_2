package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.TipoComida;
import vos.Video;

public class DAOTipoComida {

	/**
	 * Arraylits de recursos que se usan para la ejecución de sentencias SQL
	 */
	private ArrayList<Object> recursos;

	/**
	 * Atributo que genera la conexión a la base de datos
	 */
	private Connection conn;

	/**
	 * Metodo constructor que crea DAOVideo
	 * <b>post: </b> Crea la instancia del DAO e inicializa el Arraylist de recursos
	 */
	public DAOTipoComida() {
		recursos = new ArrayList<Object>();
	}

	/**
	 * Metodo que cierra todos los recursos que estan enel arreglo de recursos
	 * <b>post: </b> Todos los recurso del arreglo de recursos han sido cerrados
	 */
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	/**
	 * Metodo que inicializa la connection del DAO a la base de datos con la conexión que entra como parametro.
	 * @param con  - connection a la base de datos
	 */
	public void setConn(Connection con){
		this.conn = con;
	}
	
	/**
	 * 
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public ArrayList<TipoComida> darTiposComida() throws SQLException, Exception {
		ArrayList<TipoComida> list = new ArrayList<TipoComida>();

		String sql = "SELECT * FROM TIPO_COMIDA_REST";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			long id = rs.getLong("ID_TIPO_COMIDA");
			String tipo = rs.getString("TIPO_COMIDA");
			list.add(new TipoComida(id,tipo));
		}
		return list;
	}
	
	public void addTipoComida(TipoComida tipo) throws SQLException, Exception {

		String sql = "INSERT INTO TIPO_COMIDA_REST VALUES (";
		sql += tipo.getId() + ",'";
		sql += tipo.getTipo() + "')";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();

	}
	
	public void deleteTipoComida (TipoComida tipo) throws SQLException
	{
		String sql = "DELETE FROM TIPO_COMIDA_REST WHERE ID_TIPO_COMIDA = " + tipo.getId();
		
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prep.executeQuery();
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		conn.commit();
		conn.commit();
	}
}
