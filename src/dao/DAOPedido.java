package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import vos.Pedido;
import vos.Pedido_Producto;
import vos.Zona;

public class DAOPedido {

	private ArrayList<Object> recursos;
	private Connection conn;


	public DAOPedido() {
		recursos = new ArrayList<>();
	}

	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con){
		this.conn = con;
	}


	public void addPedido(Pedido_Producto pedido) throws SQLException, Exception {

		long  stock = 0;
		String sqlpre = "select STOCK from PLATOS where ID_PRODUCTO=" + pedido.getProducto()
		+ " AND NIT_RESTAURANTE=" + pedido.getNit_restaurante();
		System.out.println(sqlpre);

		PreparedStatement prepStmtpre = conn.prepareStatement(sqlpre);
		recursos.add(prepStmtpre);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet rs = prepStmtpre.executeQuery();

		while(rs.next()) {
			stock = rs.getLong("STOCK");
		}

		if(stock>0) {

			String sql2 = "INSERT INTO PEDIDO VALUES ('";
			sql2 += pedido.getFecha() + "',";
			sql2 += pedido.getMesa() + ",";
			sql2 += pedido.getEstadoPedido() + ")";
			System.out.println(sql2);
			PreparedStatement prepStmt2 = conn.prepareStatement(sql2);
			recursos.add(prepStmt2);
			prepStmt2.executeQuery();

			String sql = "INSERT INTO PEDIDO_PRODUCTO VALUES ('";
			sql += pedido.getFecha() + "',";
			sql += pedido.getMesa() + ",";
			sql += pedido.getNit_restaurante() + ",";
			sql += pedido.getProducto() + ",";
			sql += pedido.getCantidadUniandes() + ")";
			System.out.println(sql);
			PreparedStatement prepStmt = conn.prepareStatement(sql);
			recursos.add(prepStmt);
			prepStmt.executeQuery();
			conn.commit();
		}else {
			System.out.println("No hay stock del producto en el restautaurante");
		}
	}

	//////////////////////////////////////////////////////
	//Registra pedido con equivalencia
	////////////////////////////////////////////////////

	public void addPedidoEquivalente(Pedido_Producto pedido) throws SQLException, Exception {

		long  stock = 0;
		boolean puede = false;

		//VERIFICO SI el producto y su equivalente efectivamente son equivalentes
		
		String sqlpre2 = "select ID_EQUIVALENCIA from PRODUCTO_EQUIVALENCIA where ID_PRODUCTO=" + pedido.getProducto()
		+ " AND NIT_RESTAURANTE=" + pedido.getNit_restaurante()+" AND ID_EQUIVALENCIA = "+pedido.getEquivalente();
		System.out.println(sqlpre2);

		PreparedStatement prepStmtpre2 = conn.prepareStatement(sqlpre2);
		recursos.add(prepStmtpre2);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet rs2 = prepStmtpre2.executeQuery();

		if(rs2 != null) {
			puede = true;
		}
		
		//Verifico el Stock del producto
		
		String sqlpre = "select STOCK from PLATOS where ID_PRODUCTO=" + pedido.getProducto()
		+ " AND NIT_RESTAURANTE=" + pedido.getNit_restaurante();
		System.out.println(sqlpre);

		PreparedStatement prepStmtpre = conn.prepareStatement(sqlpre);
		recursos.add(prepStmtpre);
		ResultSet rs = prepStmtpre.executeQuery();

		while(rs.next()) {
			stock = rs.getLong("STOCK");
		}
		System.out.println(puede);
		System.out.println(stock);
		//Ejecuta la sentencia si ambas cosas se cumplen
		if(stock>0 && puede) {

			String sql2 = "INSERT INTO PEDIDO VALUES ('";
			sql2 += pedido.getFecha() + "',";
			sql2 += pedido.getMesa() + ",";
			sql2 += pedido.getEstadoPedido() + ")";
			System.out.println(sql2);
			PreparedStatement prepStmt2 = conn.prepareStatement(sql2);
			recursos.add(prepStmt2);
			prepStmt2.executeQuery();

			String sql = "INSERT INTO PEDIDO_PRODUCTO VALUES ('";
			sql += pedido.getFecha() + "',";
			sql += pedido.getMesa() + ",";
			sql += pedido.getNit_restaurante() + ",";
			sql += pedido.getProducto() + ",";
			sql += pedido.getCantidadUniandes() + ",";
			sql += pedido.getEquivalente() + ")";
			System.out.println(sql);
			PreparedStatement prepStmt = conn.prepareStatement(sql);
			recursos.add(prepStmt);
			prepStmt.executeQuery();
			conn.commit();
		}else {
			System.out.println("No hay stock del producto en el restautaurante o los productos no son equivalentes");
		}
	}
	
	//Registra una lista de pedidos de una mesa 
	
	public void addPedidoList(ArrayList<Pedido_Producto> list)  throws SQLException, Exception {
		for(Pedido_Producto p : list)
			addPedidoEquivalente(p);
	}
	
	/**
	 * 
	 * @param marcar
	 * @throws SQLException
	 */
	public void marcarCompleto(Pedido_Producto marcar) throws SQLException
	{
		
		//marcar el pedido como entregado : UPDATE PEDIDO SET PEDIDO.ESTADO = 1 WHERE PEDIDO.FECHA = '2017-10-10' AND NUM_MESA = 1 ;
		String sql1 = "UPDATE PEDIDO SET PEDIDO.ESTADO = 1" + " WHERE PEDIDO.FECHA = '" + marcar.getFecha() + "' AND NUM_MESA = " +
		marcar.getMesa();
		System.out.println(sql1);
		
		//disminuir el stock en menu POR LA CANTIDAD : 
		//UPDATE MENU SET MENU.STOCK = MENU.STOCK - 1  WHERE ID_PRODUCTO = 1 AND NIT_RESTAURANTE = 1;
		String sql2 = "UPDATE MENU SET MENU.STOCK = MENU.STOCK - " + marcar.getCantidadUniandes() + " WHERE ID_PRODUCTO = " +
		marcar.getProducto() + " AND NIT_RESTAURANTE = " + marcar.getNit_restaurante();
		System.out.println(sql2);
		
		//disminuir el stock en plato por la cantidad :
		//UPDATE PLATOS SET PLATOS.STOCK = PLATOS.STOCK - 10 WHERE ID_PRODUCTO = 1 AND NIT_RESTAURANTE = 1 ;
		String sql3 = "UPDATE PLATOS SET PLATOS.STOCK = PLATOS.STOCK - " + marcar.getCantidadUniandes() + " WHERE ID_PRODUCTO = " + 
		marcar.getProducto() + " AND NIT_RESTAURANTE = " + marcar.getNit_restaurante();
		System.out.println(sql3);
		
		PreparedStatement prepStmt1 = conn.prepareStatement(sql1);
		recursos.add(prepStmt1);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt1.executeQuery();
		
		PreparedStatement prepStmt2 = conn.prepareStatement(sql2);
		recursos.add(prepStmt2);
		prepStmt2.executeQuery();
		
		PreparedStatement prepStmt3 = conn.prepareStatement(sql3);
		recursos.add(prepStmt3);
		prepStmt3.executeQuery();
		conn.commit();
	}

	/**
	 * 
	 * @param pedido
	 * @throws SQLException
	 * @throws Exception
	 */
	//Cancela un pedido
	public void cancelarPedido(Pedido_Producto pedido)  throws SQLException, Exception {
		
		//Cambio el estado del pedido
		String sql = "update PEDIDO set ESTADO = 3 where "
				+ " FECHA='"+ pedido.getFecha() 
				+"' AND NUM_MESA = "+pedido.getMesa();
		System.out.println(sql);
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		
		//Cambio el stock en la tabla de platos por restaurante
		String sql2 ="update PLATOS set "
				+ " STOCK = STOCK + "+pedido.getCantidadUniandes()
				+ " where ID_PRODUCTO = "+pedido.getProducto()
				+ " AND "
				+ " NIT_RESTAURANTE = "+pedido.getNit_restaurante();
		System.out.println(sql2);

		PreparedStatement prepStmt2 = conn.prepareStatement(sql2);
		recursos.add(prepStmt2);

		prepStmt2.executeQuery();
		

		//Cambio el stock en la tabla Menu del restaurante
		String sql3 = "UPDATE MENU SET "
				+ " MENU.STOCK = MENU.STOCK + " +pedido.getCantidadUniandes()
				+ " WHERE ID_PRODUCTO = "+pedido.getProducto()
				+ " AND NIT_RESTAURANTE = " + pedido.getNit_restaurante();
		
		System.out.println(sql3);
		PreparedStatement prepStmt3 = conn.prepareStatement(sql3);
		recursos.add(prepStmt3);
		prepStmt3.executeQuery();
		conn.commit();
		
	}
	
	public void ListCompleto(ArrayList<Pedido_Producto> marcar) throws SQLException
	{
		for(Pedido_Producto pedido : marcar)
		{
			marcarCompleto(pedido);
		}
	}
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public ArrayList<Pedido_Producto> pedidosDeLogueados() throws SQLException
	{
		//SELECT PEDIDO_PRODUCTO.FECHA, PEDIDO_PRODUCTO.NUM_MESA,PEDIDO_PRODUCTO.NIT_REST,PEDIDO_PRODUCTO.ID_PRODUCTO,PEDIDO_PRODUCTO.CANTIDAD 
		//FROM PEDIDO_PRODUCTO INNER JOIN LOGUEADO ON PEDIDO_PRODUCTO.NUM_MESA = LOGUEADO.NUM_MESA ORDER BY NIT_REST;
		ArrayList<Pedido_Producto> resp = new ArrayList<Pedido_Producto>(); 
		String sql = "SELECT PEDIDO_PRODUCTO.FECHA, PEDIDO_PRODUCTO.NUM_MESA,PEDIDO_PRODUCTO.NIT_REST,PEDIDO_PRODUCTO.ID_PRODUCTO,PEDIDO_PRODUCTO.CANTIDAD FROM " + 
					"PEDIDO_PRODUCTO INNER JOIN LOGUEADO ON PEDIDO_PRODUCTO.NUM_MESA = LOGUEADO.NUM_MESA ORDER BY NIT_REST";
		System.out.println(sql);
					
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		ResultSet rs =prepStmt.executeQuery();
		conn.commit();
		
		while(rs.next())
		{
			String fecha =  rs.getString("FECHA");
			long mesa = rs.getLong("NUM_MESA");
			long nit = rs.getLong("NIT_REST");
			long producto = rs.getLong("ID_PRODUCTO");
			long cantidad = rs.getLong("CANTIDAD");
			
			resp.add(new Pedido_Producto(fecha, mesa, 0, cantidad, 0,producto , nit, 0));
		}
		
		
		return resp;
	}
	
	
}
