package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.UsuarioCliente;
import vos.UsuarioGeneral;
import vos.RolUsuario;


public class DAOTablaCliente {

	private ArrayList<Object> recursos;
	private Connection conn;
	
	public DAOTablaCliente() {
		recursos = new ArrayList<>();
	}
	
	
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con){
		this.conn = con;
	}
	
	public ArrayList<UsuarioCliente> darUsuarios() throws SQLException, Exception {
		ArrayList<UsuarioCliente> usuarios = new ArrayList<UsuarioCliente>();

		String sql = "SELECT *" + 
				"    FROM (((USUARIO INNER JOIN CLIENTE ON NUM_DOCUMENTO = ID_USUARIO) INNER JOIN ZONA_PREF ON CLIENTE.ID_USUARIO = ZONA_PREF.ID_USUARIO)" + 
				"    INNER JOIN TIPO_C_PREFERIDA ON USUARIO.NUM_DOCUMENTO = TIPO_C_PREFERIDA.ID_USUARIO) INNER JOIN RESERVA ON NUM_DOCUMENTO = RESERVA.ID_USUARIO";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nombre = rs.getString("NOMBRE");
			String contraseña = rs.getString("PASSWORD");
			String userName = rs.getString("USER_NAME");
			String email = rs.getString("EMAIL");
			String tipoUsuario = rs.getString("TIPO_USUARIO");
			String identificacion = rs.getString("NUM_DOCUMENTO");
			Double prefPrecio = rs.getDouble("PREF_PRECIO");
			Long prefCategoria = rs.getLong("ID_TIPO_COMIDA");
			Long idReserva = rs.getLong("ID_RESERVA");
			Long numMesa = 0L;
			String zona = rs.getString("NOM_ZONA");
			UsuarioCliente nuevo = new UsuarioCliente(userName,contraseña,nombre,email,
					identificacion,darRol(tipoUsuario), prefPrecio, prefCategoria,idReserva,numMesa,zona);
			usuarios.add(nuevo);
		}
		return usuarios;
	}
	
	public void addCliente(UsuarioCliente usuarioCliente) throws SQLException, Exception {

		Long identificacion = Long.valueOf(usuarioCliente.getIdentificacion());
		String sql = "INSERT INTO USUARIO VALUES (";
		sql += identificacion + " ,";
		sql += "'"+usuarioCliente.getUsuario() + "' ,";
		sql += "'"+ "UsuarioCliente" + "' ,";
		sql += "'"+usuarioCliente.getContraseña() + "' ,";
		sql += "'"+usuarioCliente.getNombre() + "' ,";
		sql += "'"+usuarioCliente.getEmail() + "' )";
		System.out.println(sql);
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		
		String sql2 = "INSERT INTO CLIENTE VALUES (";
		sql2 += identificacion + ",";
		sql2 += usuarioCliente.getPreferenciaPrecio() + ")";
		System.out.println(sql2);
		PreparedStatement prepStmt2 = conn.prepareStatement(sql2);
		recursos.add(prepStmt2);
		prepStmt2.executeQuery();
		
		
		String sql3 = "INSERT INTO ZONA_PREF VALUES (";
		sql3 += identificacion + ",";
		sql3 += "'"+usuarioCliente.getZonas() + "')";
		System.out.println(sql3);
		PreparedStatement prepStmt3 = conn.prepareStatement(sql3);
		recursos.add(prepStmt3);
		prepStmt3.executeQuery();
		conn.commit();
		
		
	}
	
	public void updateUsuario(UsuarioGeneral usuario) throws SQLException, Exception {

		String sql = "UPDATE USUARIO SET ";
		sql += "USER_NAME='" + usuario.getUsuario() + "',";
		sql += "TIPO_USUARIO='" + "UsuarioCliente" + "',";
		sql += "PASSWORD='" + usuario.getContraseña() + "',";
		sql += "NOMBRE=" + usuario.getNombre();
		sql += "EMAIL=" + usuario.getEmail();
		sql += " WHERE NUM_DOCUMENTO = " + "'"+usuario.getIdentificacion()+"'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();
		
	}
	
	
	private RolUsuario darRol(String rol){
		if(rol.equals("UsuarioResturante")) return RolUsuario.UsuarioResturante;
		if(rol.equals("UsuarioCliente")) return RolUsuario.UsuarioCliente;
		if(rol.equals("UsuarioAdministrador")) return RolUsuario.UsuarioAdministrador;
		return null;
	}

	
}
