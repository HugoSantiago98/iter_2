package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import vos.Ingrediente;

public class DAOIngrediente {
	
	private ArrayList<Object> recursos;
	
	private Connection conn;
	
	public DAOIngrediente () {
		recursos = new ArrayList<Object> ();
	}
	
	
	public void cerrarResources ()
	{
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}
	
	public void setConn(Connection con)
	{
		conn = con;
	}
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public void addIngrediente(Ingrediente ing) throws SQLException
	{
		
		
		String sql = "INSERT INTO INGREDIENTE VALUES (";
		sql += ing.getId() + ",";
		sql += ing.getRestaurante() + ",'";
		sql += ing.getDescripcion() + "','";
		sql += ing.getDescripcionEN() + "',";
		sql += ing.getPrecio() + ",";
		sql += ing.getStock() + ",'";
		sql += ing.getNombre() + "')";
		
		System.out.println(sql);
		PreparedStatement stm = conn.prepareStatement(sql);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		recursos.add(stm);
		stm.executeQuery();
		conn.commit();
	}
	
	
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public List<Ingrediente> getIngredientes () throws SQLException, Exception
	{
		ArrayList<Ingrediente> ingredientes = new ArrayList<Ingrediente>();

		String sql = "SELECT * FROM INGREDIENTE";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			long id = rs.getLong("ID_INGREDIENTE");
			String nombre = rs.getString("NOMBRE");
			String descripcion = rs.getString("DESCRIPCION");
			String descripcionENG = rs.getString("DESCRIPCION_EN");
			double precio = rs.getDouble("PRECIO");
			double stock = rs.getDouble("STOCK");
			long restaurante = rs.getLong("NIT_RESTAURANTE");
			ingredientes.add(new Ingrediente(id, nombre, descripcion, descripcionENG, precio, stock, restaurante));
			
		}
		return ingredientes;
	}

}
