package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import vos.Ingrediente;
import vos.Ingrediente_Equivalente;

public class DAOIngrediente_Equivalente {

private ArrayList<Object> recursos;
	
	private Connection conn;
	
	public DAOIngrediente_Equivalente () {
		recursos = new ArrayList<Object> ();
	}
	
	
	public void cerrarResources ()
	{
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}
	
	public void setConn(Connection con)
	{
		conn = con;
	}
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public void addIngredienteEquivalente(Ingrediente_Equivalente ing) throws SQLException
	{
		
		String sql = "INSERT INTO INGREDIENTE_EQUIVALENTE VALUES (";
		sql += ing.getId() + ",";
		sql += ing.getEquivalente() + ",";
		sql += ing.getRestaurante() + ")";
		
		System.out.println(sql);
		PreparedStatement stm = conn.prepareStatement(sql);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		recursos.add(stm);
		stm.executeQuery();
		conn.commit();
	}
	
}
