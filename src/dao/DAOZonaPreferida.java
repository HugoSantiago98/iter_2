package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.Restaurante;
import vos.ZonaPreferida;

public class DAOZonaPreferida {
	
private ArrayList<Object> recursos;
	
	private Connection conn;
	
	public DAOZonaPreferida () {
		recursos = new ArrayList<Object> ();
	}
	
	
	public void cerrarResources ()
	{
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}
	
	public void setConn(Connection con)
	{
		conn = con;
	}

	
	//----------------------/ / --------------------------------//
	//                      Transacciones
	
	/**
	 *Metodo que devuelve todos los registros de zonas preferidas por usuarios 
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public ArrayList<ZonaPreferida> darPreferencias() throws SQLException, Exception {
		ArrayList<ZonaPreferida> rest = new ArrayList<ZonaPreferida>();

		String sql = "SELECT * FROM ZONA_PREF";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			long usuario = rs.getLong("ID_USUARIO");
			String zona = rs.getString("NOM_ZONA");
			long id = rs.getLong("ID");
			
			rest.add(new ZonaPreferida(usuario, zona, id));
		}
		return rest;
	}
	
	/**
	 * Metodo que agrega un registro de una zona preferida por un usuario
	 */
	public void addZonaPreferida(ZonaPreferida rest) throws SQLException, Exception 
	{
		//INSERT INTO ZONA_PREF VALUES (26,'zonas',1);
		
		String sql = "INSERT INTO ZONA_PREF VALUES (";
		sql += rest.getIdu() + ",'";
		sql += rest.getZona() + "',";
		sql += rest.getId() + ")";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();
	}
	
	/**
	 * TODO METODO PARA BORRAR UN REGISTRO DE UNA ZONA PREFERIDA
	 */
	public void deleteZonaPreferida (ZonaPreferida rest) throws SQLException
	{
		//
		String sql = "DELETE FROM ZONA_PREF WHERE ID = " + rest.getId();
		
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prep.executeQuery();
		conn.commit();
	}
	
	/**
	 * metodo para actualizar un registro de una preferencia
	 */
	public void updateZona (ZonaPreferida rest) throws SQLException
	{
		//TODO UPDATE ZONA_PREF SET ID_USUARIO = 2, NOM_ZONA = 'las buenas drogas' WHERE ID_USUARIO = 1 AND NOM_ZONA = 'zona'; 
		//ME PARECE PRUDENTE AGREGARLE UN ID AL REGISTRO, ASI SE EXACTAMENTE CUAL ES EL QUE VOY A MODIFICAR
		String sql = "UPDATE ZONA_PREF SET ID_USUARIO = " + rest.getIdu() + ", NOM_ZONA = '" + rest.getZona() + "'"+ 
				"WHERE ID =" + rest.getId() ;
		
		System.out.println(sql);
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prep.executeQuery();
		conn.commit();
	}
}
