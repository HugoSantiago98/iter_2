package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import vos.Plato;
import vos.Producto;
import vos.RolUsuario;
import vos.TipoPlato;
import vos.UsuarioGeneral; 

public class DAOTablaProducto {

	private ArrayList<Object> recursos;
	private Connection conn;


	public DAOTablaProducto() {
		recursos = new ArrayList<>();
	}

	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con){
		this.conn = con;
	}

	public void addProducto(Plato plato, Long idUsuario) throws SQLException, Exception {
		

			SimpleDateFormat a = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

			String preSQL = "alter session set nls_date_format = 'yyyy-mm-dd hh:mi:ss'";
			PreparedStatement prepStmtDATE = conn.prepareStatement(preSQL);
			recursos.add(prepStmtDATE);
			conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			prepStmtDATE.executeQuery();


			String sql2 = "INSERT INTO PRODUCTO VALUES (";
			sql2 += plato.getNitrestaurante() + ",";
			sql2 += plato.getIdPlato()+",";
			sql2 += "'"+plato.getNombre() + "' ,";
			sql2 += "'"+plato.getTipoPlato().toString() + "' ,";
			sql2 += darCategoria(plato.getTipoPlato())+ ")";

			System.out.println(sql2);

			PreparedStatement prepStmt2 = conn.prepareStatement(sql2);
			recursos.add(prepStmt2);
			prepStmt2.executeQuery();

			String sql = ""
					+ "INSERT INTO PLATOS VALUES (";
			sql += plato.getIdPlato()+",";
			sql += plato.getNitrestaurante() + ",";
			sql += plato.getStock() + ",";
			sql += plato.getCostoPreparacion() + ",";
			sql += plato.getTiempoPreparacion() + ",";
			sql += "'" + plato.getDescripcion() + "' ,";
			sql += "'" + plato.getDescripcionEN() + "' ,";
			sql += plato.getPrecioVenta() + ",";
			sql += plato.getVendidos() + ",";
			sql += "'"+a.format(plato.getFechaInicio())+ "' ,";
			sql += "'"+a.format(plato.getFechaFin())+ "' ,";
			sql += darCategoria(plato.getTipoPlato())+ ",";
			sql += plato.getIdMenu() + ")";
			
			System.out.println(sql);

			PreparedStatement prepStmt = conn.prepareStatement(sql);
			recursos.add(prepStmt);
			prepStmt.executeQuery();
			conn.commit();

	}

	private Long darCategoria(TipoPlato plato) {
		if(plato.equals(TipoPlato.PLATO_FUERTE)) return 1L;
		if(plato.equals(TipoPlato.BEBIDA)) return 2L;
		if(plato.equals(TipoPlato.POSTRE)) return 3L;
		if(plato.equals(TipoPlato.ENTRADA)) return 4L;
		if(plato.equals(TipoPlato.ACOMPAŅAMIENTO)) return 5L;
		return null;

	}

}
