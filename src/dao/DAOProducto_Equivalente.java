package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.Ingrediente_Equivalente;
import vos.Producto_Equivalente;

public class DAOProducto_Equivalente {

	private Connection conn;
	private ArrayList<Object> recursos;
	public DAOProducto_Equivalente () {
		recursos = new ArrayList<Object> ();
	}
	public void cerrarResources ()
	{
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con)
	{
		conn = con;
	}
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public void addProductoEquivalente(Producto_Equivalente ing) throws SQLException
	{
		
		String sql = "INSERT INTO PRODUCTO_EQUIVALENCIA VALUES (";
		sql += ing.getId() + ",";
		sql += ing.getEquivalente() + ",";
		sql += ing.getNit() + ")";
		
		System.out.println(sql);
		PreparedStatement stm = conn.prepareStatement(sql);
		recursos.add(stm);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		stm.executeQuery();
		conn.commit();
	}
}
