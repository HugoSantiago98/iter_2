package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import vos.Menu;
import vos.Pedido;

public class DAOMenu {

	private ArrayList<Object> recursos;
	private Connection conn;
	
	public DAOMenu() {
		recursos = new ArrayList<>();
	}
	
	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con){
		this.conn = con;
	}

	public void addMenu(Menu menu) throws SQLException, Exception {

				
		String preSQL = "alter session set nls_date_format = 'yyyy-mm-dd'";
		PreparedStatement prepStmtDATE = conn.prepareStatement(preSQL);
		recursos.add(prepStmtDATE);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmtDATE.executeQuery();
		
		String sql = "INSERT INTO MENU VALUES (";
		sql += menu.getIdPlato() + ",";
		sql += menu.getNitrestaurante() + ",";
		sql += menu.getStock() + ",'";
		sql += menu.getDescripcion() + "','";
		sql += menu.getDescripcionEN() + "',";
		sql += menu.getPrecioVenta() + ",";
		sql += menu.getVendidos() + ",'";
		sql += menu.getFechaInicio() + "','";
		sql += menu.getFechaFin() + "',";
		sql += menu.getPlatoEntrada() + ",";
		sql += menu.getPlatoPostre() + ",";
		sql += menu.getPlatoBebida() + ",";
		sql += menu.getPlatoFuerte() + ",";
		sql += menu.getPlatoAcom() + ")";
		System.out.println(sql);
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		prepStmt.executeQuery();
		conn.commit();
	}
}
