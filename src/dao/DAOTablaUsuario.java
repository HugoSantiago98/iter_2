package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.RolUsuario;
import vos.UsuarioGeneral;



public class DAOTablaUsuario {


	private ArrayList<Object> recursos;
	private Connection conn;


	public DAOTablaUsuario() {
		recursos = new ArrayList<>();
	}

	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con){
		this.conn = con;
	}

	public ArrayList<UsuarioGeneral> darUsuarios() throws SQLException, Exception {
		ArrayList<UsuarioGeneral> usuarios = new ArrayList<UsuarioGeneral>();

		String sql = "SELECT * FROM USUARIO";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nombre = rs.getString("NOMBRE");
			String contraseña = rs.getString("PASSWORD");
			String userName = rs.getString("USER_NAME");
			String email = rs.getString("EMAIL");
			String tipoUsuario = rs.getString("TIPO_USUARIO");
			String identificacion = rs.getString("NUM_DOCUMENTO");
			UsuarioGeneral nuevo = new UsuarioGeneral(userName,contraseña,nombre,email,identificacion,darRol(tipoUsuario));
			usuarios.add(nuevo);
		}
		return usuarios;
	}

	public UsuarioGeneral buscarUsuarioPorIdentificacion(String pidentificacion) throws SQLException, Exception {
		UsuarioGeneral usuarios = null;

		String sql = "SELECT * FROM USUARIO WHERE NUM_DOCUMENTO ='" + pidentificacion + "'";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nombre = rs.getString("NOMBRE");
			String contraseña = rs.getString("PASSWORD");
			String userName = rs.getString("USER_NAME");
			String email = rs.getString("EMAIL");
			String tipoUsuario = rs.getString("TIPO_USUARIO");
			String identificacion = rs.getString("NUM_DOCUMENTO");
			usuarios = new UsuarioGeneral(userName,contraseña,nombre,email,identificacion,darRol(tipoUsuario));
		}
		return usuarios;
	}

	public void addUsuario(UsuarioGeneral usuario) throws SQLException, Exception {

		Long identificacion = Long.valueOf(usuario.getIdentificacion());
		String sql = "INSERT INTO USUARIO VALUES (";
		sql += identificacion + " ,";
		sql += "'"+usuario.getUsuario() + "' ,";
		sql += "'"+usuario.getRolUsuario().toString() + "' ,";
		sql += "'"+usuario.getContraseña() + "' ,";
		sql += "'"+usuario.getNombre() + "' ,";
		sql += "'"+usuario.getEmail() + "' )";
		
		System.out.println(sql);

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();
	}
	
	public void updateUsuario(UsuarioGeneral usuario) throws SQLException, Exception {

		String sql = "UPDATE USUARIO SET ";
		sql += "USER_NAME=" + "'"+usuario.getUsuario() + "',";
		sql += "TIPO_USUARIO=" + "'"+usuario.getRolUsuario().toString() + "',";
		sql += "PASSWORD=" + "'"+usuario.getContraseña() + "',";
		sql += "NOMBRE=" + "'"+usuario.getNombre()+ "',";
		sql += "EMAIL=" + "'"+usuario.getEmail()+ "'";
		sql += " WHERE NUM_DOCUMENTO = " + usuario.getIdentificacion();

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();
	}
	
	public void deleteUsuario(UsuarioGeneral usuario) throws SQLException, Exception {

		String sql = "DELETE FROM USUARIO";
		sql += " WHERE NUM_DOCUMENTO = " + usuario.getIdentificacion();

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();
	}
	
	private RolUsuario darRol(String rol){
		if(rol.equals("UsuarioResturante")) return RolUsuario.UsuarioResturante;
		if(rol.equals("UsuarioCliente")) return RolUsuario.UsuarioCliente;
		if(rol.equals("UsuarioAdministrador")) return RolUsuario.UsuarioAdministrador;
		return null;
	}

}
