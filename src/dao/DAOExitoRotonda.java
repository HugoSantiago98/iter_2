package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.sun.xml.internal.ws.api.server.AbstractInstanceResolver;

import vos.ExitoRotondes;

public class DAOExitoRotonda {

	private ArrayList<Object> recursos;
	private Connection conn;

	public DAOExitoRotonda() {
		recursos = new ArrayList<>();
	}

	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con){
		this.conn = con;
	}
	

	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<ExitoRotondes> darExitosos () throws SQLException {
		ArrayList<ExitoRotondes> res = new ArrayList<>();
		res.clear();
		ExitoRotondes lunes = new ExitoRotondes("", 0, 0, 0, 0, 0, 0, 0, 0);
		res.add(lunes);
		ExitoRotondes martes = new ExitoRotondes("", 0, 0, 0, 0, 0, 0, 0, 0);
		res.add(martes);
		ExitoRotondes miercoles = new ExitoRotondes("", 0, 0, 0, 0, 0, 0, 0, 0);
		res.add(miercoles);
		ExitoRotondes jueves = new ExitoRotondes("", 0, 0, 0, 0, 0, 0, 0, 0);
		res.add(jueves);
		ExitoRotondes viernes = new ExitoRotondes("", 0, 0, 0, 0, 0, 0, 0, 0);
		res.add(viernes);
		ExitoRotondes sabado = new ExitoRotondes("", 0, 0, 0, 0, 0, 0, 0, 0);
		res.add(sabado);
		ExitoRotondes domingo = new ExitoRotondes("", 0, 0, 0, 0, 0, 0, 0, 0);
		res.add(domingo);

		String sql1 = "select tabla1.DIA, tabla1.NIT_REST,b.MasFrecuentado from " + 
				"(select DIA,NIT_REST,count(NIT_REST) Frecuentan from( " + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,NIT_REST ) tabla1 " + 
				" inner join " + 
				"(select DIA,MAX(Frecuencia) MasFrecuentado from (" + 
				"(select DIA,NIT_REST,count(NIT_REST)Frecuencia from(" + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,NIT_REST)) " + 
				"group by DIA) b on tabla1.DIA=b.DIA AND tabla1.Frecuentan=b.MasFrecuentado";
		System.out.println(sql1);
		PreparedStatement prepStmt = conn.prepareStatement(sql1);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		//Restaurantes mas frecuentados

		while(rs.next()) {
			String dia = rs.getString("DIA");
			Long nit = rs.getLong("NIT_REST");
			Long cantidad = rs.getLong("MASFRECUENTADO");
			if(dia.equals("LUNES    ")) {
				res.get(0).setD�a(dia);
				res.get(0).setNit_Restaurante(nit);
				res.get(0).setFrecuenciaRestaurante(cantidad);
			}
			if(dia.equals("MARTES   ")) {
				res.get(1).setD�a(dia);
				res.get(1).setNit_Restaurante(nit);
				res.get(1).setFrecuenciaRestaurante(cantidad);
			}
			if(dia.equals("MI�RCOLES")) {
				res.get(2).setD�a(dia);
				res.get(2).setNit_Restaurante(nit);
				res.get(2).setFrecuenciaRestaurante(cantidad);
			}
			if(dia.equals("JUEVES   ")) {
				res.get(3).setD�a(dia);
				res.get(3).setNit_Restaurante(nit);
				res.get(3).setFrecuenciaRestaurante(cantidad);
			}
			if(dia.equals("VIERNES  ")) {
				res.get(4).setD�a(dia);
				res.get(4).setNit_Restaurante(nit);
				res.get(4).setFrecuenciaRestaurante(cantidad);
			}
			if(dia.equals("S�BADO   ")) {
				res.get(5).setD�a(dia);
				res.get(5).setNit_Restaurante(nit);
				res.get(5).setFrecuenciaRestaurante(cantidad);
			}
			if(dia.equals("DOMINGO  ")) {
				res.get(6).setD�a(dia);
				res.get(6).setNit_Restaurante(nit);
				res.get(6).setFrecuenciaRestaurante(cantidad);
			}
		}

		//Restaurantes menos frecuentados
		
		String sql2 ="select tabla1.DIA, tabla1.NIT_REST,b.MasFrecuentado from " + 
				"(select DIA,NIT_REST,count(NIT_REST) Frecuentan from( " + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,NIT_REST ) tabla1 " + 
				" inner join " + 
				"(select DIA,MIN(Frecuencia) MasFrecuentado from (" + 
				"(select DIA,NIT_REST,count(NIT_REST)Frecuencia from(" + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,NIT_REST)) " + 
				"group by DIA) b on tabla1.DIA=b.DIA AND tabla1.Frecuentan=b.MasFrecuentado";
		
		PreparedStatement prepStmt2 = conn.prepareStatement(sql2);
		recursos.add(prepStmt2);
		ResultSet rs2 = prepStmt2.executeQuery();
		
		while(rs2.next()) {
			String dia = rs2.getString("DIA");
			Long nit = rs2.getLong("NIT_REST");
			Long cantidad = rs2.getLong("MASFRECUENTADO");
			if(dia.equals("LUNES    ")) {
				res.get(0).setNit_Restaurante_Menor(nit);
				res.get(0).setFrecuenciaRestaurante_Menor(cantidad);
			}
			if(dia.equals("MARTES   ")) {
				res.get(1).setNit_Restaurante_Menor(nit);
				res.get(1).setFrecuenciaRestaurante_Menor(cantidad);
			}
			if(dia.equals("MI�RCOLES")) {
				res.get(2).setNit_Restaurante_Menor(nit);
				res.get(2).setFrecuenciaRestaurante_Menor(cantidad);
			}
			if(dia.equals("JUEVES   ")) {
				res.get(3).setNit_Restaurante_Menor(nit);
				res.get(3).setFrecuenciaRestaurante_Menor(cantidad);
			}
			if(dia.equals("VIERNES  ")) {
				res.get(4).setNit_Restaurante_Menor(nit);
				res.get(4).setFrecuenciaRestaurante_Menor(cantidad);
			}
			if(dia.equals("S�BADO   ")) {
				res.get(5).setNit_Restaurante_Menor(nit);
				res.get(5).setFrecuenciaRestaurante_Menor(cantidad);
			}
			if(dia.equals("DOMINGO  ")) {
				res.get(6).setNit_Restaurante_Menor(nit);
				res.get(6).setFrecuenciaRestaurante_Menor(cantidad);
			}
		}
		
		//Productos mas vendidos
		
		String sql3 ="select ac.DIA, ac.ID_PRODUCTO,b.MasVendido from " + 
				"(select DIA,ID_PRODUCTO,SUM(CANTIDAD) VENDIDOS from( " + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,ID_PRODUCTO ) ac " + 
				" inner join " + 
				"(select DIA,MAX(VENDIDOS) MasVendido from (" + 
				"(select DIA,ID_PRODUCTO,SUM(CANTIDAD) VENDIDOS from(" + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,ID_PRODUCTO))" + 
				"group by DIA) b on ac.DIA=b.DIA AND ac.VENDIDOS=b.MasVendido";
		
		PreparedStatement prepStmt3 = conn.prepareStatement(sql3);
		recursos.add(prepStmt3);
		ResultSet rs3 = prepStmt3.executeQuery();
		
		while(rs3.next()) {
			String dia = rs3.getString("DIA");
			Long nit = rs3.getLong("ID_PRODUCTO");
			Long cantidad = rs3.getLong("MASVENDIDO");
			if(dia.equals("LUNES    ")) {
				res.get(0).setiD_Producto(nit);
				res.get(0).setProductosVendidos(cantidad);
			}
			if(dia.equals("MARTES   ")) {
				res.get(1).setiD_Producto(nit);
				res.get(1).setProductosVendidos(cantidad);
			}
			if(dia.equals("MI�RCOLES")) {
				res.get(2).setiD_Producto(nit);
				res.get(2).setProductosVendidos(cantidad);
			}
			if(dia.equals("JUEVES   ")) {
				res.get(3).setiD_Producto(nit);
				res.get(3).setProductosVendidos(cantidad);
			}
			if(dia.equals("VIERNES  ")) {
				res.get(4).setiD_Producto(nit);
				res.get(4).setProductosVendidos(cantidad);
			}
			if(dia.equals("S�BADO   ")) {
				res.get(5).setiD_Producto(nit);
				res.get(5).setProductosVendidos(cantidad);
			}
			if(dia.equals("DOMINGO  ")) {
				res.get(6).setiD_Producto(nit);
				res.get(6).setProductosVendidos(cantidad);
			}
		}
		
		//Productos menos vendidos
		
		String sql4 ="select ac.DIA, ac.ID_PRODUCTO,b.Menos MenosVendidos from" + 
				"(select DIA,ID_PRODUCTO,SUM(CANTIDAD) VENDIDOS from( " + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,ID_PRODUCTO ) ac " + 
				" inner join " + 
				"(select DIA,MIN(VENDIDOS) Menos from ( " + 
				"(select DIA,ID_PRODUCTO,SUM(CANTIDAD) VENDIDOS from( " + 
				"select to_char(to_date(FECHA,'dd/mm/yyyy hh'),'DAY') DIA ,NIT_REST,ID_PRODUCTO,CANTIDAD from PEDIDO_PRODUCTO) " + 
				"group by DIA,ID_PRODUCTO)) " + 
				"group by DIA) b on ac.DIA=b.DIA AND ac.VENDIDOS=b.Menos";
		
		PreparedStatement prepStmt4 = conn.prepareStatement(sql4);
		recursos.add(prepStmt4);
		ResultSet rs4 = prepStmt4.executeQuery();
		
		while(rs4.next()) {
			String dia = rs4.getString("DIA");
			Long nit = rs4.getLong("ID_PRODUCTO");
			Long cantidad = rs4.getLong("MENOSVENDIDOS");
			if(dia.equals("LUNES    ")) {
				res.get(0).setiD_Producto_Menor(nit);
				res.get(0).setProductosMenosVendidos(cantidad);
			}
			if(dia.equals("MARTES   ")) {
				res.get(1).setiD_Producto_Menor(nit);
				res.get(1).setProductosMenosVendidos(cantidad);
			}
			if(dia.equals("MI�RCOLES")) {
				res.get(2).setiD_Producto_Menor(nit);
				res.get(2).setProductosMenosVendidos(cantidad);
			}
			if(dia.equals("JUEVES   ")) {
				res.get(3).setiD_Producto_Menor(nit);
				res.get(3).setProductosMenosVendidos(cantidad);
			}
			if(dia.equals("VIERNES  ")) {
				res.get(4).setiD_Producto_Menor(nit);
				res.get(4).setProductosMenosVendidos(cantidad);
			}
			if(dia.equals("S�BADO   ")) {
				res.get(5).setiD_Producto_Menor(nit);
				res.get(5).setProductosMenosVendidos(cantidad);
			}
			if(dia.equals("DOMINGO  ")) {
				res.get(6).setiD_Producto_Menor(nit);
				res.get(6).setProductosMenosVendidos(cantidad);
			}
		}
		
		return res;
	}


}
