package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.TipoComida;
import vos.UsuarioGeneral;
import vos.Zona;

public class DAOZona {
	
	private ArrayList<Object> recursos;
	private Connection conn;


	public DAOZona() {
		recursos = new ArrayList<>();
	}

	public void cerrarRecursos() {
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}

	public void setConn(Connection con){
		this.conn = con;
	}
	
	public ArrayList<Zona> darZonas() throws SQLException, Exception {
		ArrayList<Zona> zona = new ArrayList<Zona>();

		String sql = "SELECT * FROM ZONA";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			String nombre = rs.getString("NOMBRE");
			boolean abierto = TrueOrFalse(rs.getString("ABIERTO"));
			int capacidad = rs.getInt("CAPACIDAD");
			boolean disc = TrueOrFalse(rs.getString("DISCAPACITADOS"));
			String desc = rs.getString("DESCRIPCION");
			
			zona.add(new Zona(nombre, abierto, capacidad, disc, desc));
		}
		return zona;
	}
	
	public void addZona(Zona zona) throws SQLException, Exception {

		String sql = "INSERT INTO ZONA VALUES ('";
		sql += zona.getNombreZona() + "','";
		sql += BooleanToString(zona.isAbierto()) + "',";
		sql += zona.getCapacidad() + ",'";
		sql += BooleanToString(zona.isDiscapacitados()) + "','";
		sql += zona.getDescripcionTecnica() + "')";
		
		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();
	}
	
	private boolean TrueOrFalse (String st)
	{
		if(st.equals("T") )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private String BooleanToString (boolean t)
	{
		if(t == true) return "T";
		else return "F";
	}
	
	public void deleteZona (Zona tipo) throws SQLException
	{
		String sql = "DELETE FROM ZONA WHERE NOMBRE = '" + tipo.getNombreZona() + "'";
		
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prep.executeQuery();
		conn.commit();
	}
}
