package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import vos.InfoVO;
import vos.Plato;
import vos.Restaurante;
import vos.RolUsuario;
import vos.UsuarioGeneral;
import vos.Zona;

public class DAORestaurante {
	
private ArrayList<Object> recursos;
	
	private Connection conn;
	
	public DAORestaurante () {
		recursos = new ArrayList<Object> ();
	}
	
	
	public void cerrarResources ()
	{
		for(Object ob : recursos){
			if(ob instanceof PreparedStatement)
				try {
					((PreparedStatement) ob).close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		}
	}
	
	public void setConn(Connection con)
	{
		conn = con;
	}
	
	/**
	 * 
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	public ArrayList<Restaurante> darRestaurantes() throws SQLException, Exception {
		ArrayList<Restaurante> rest = new ArrayList<Restaurante>();

		String sql = "SELECT * FROM RESTAURANTE";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		ResultSet rs = prepStmt.executeQuery();

		while (rs.next()) {
			long id = rs.getLong("NIT");
			String nombre = rs.getString("NOMBRE");
			String pagina = rs.getString("PAGINA_WEB");
			String cuenta = rs.getString("CUENTA");
			long tipoComida = rs.getLong("ID_TIPO_COMIDA");
			long encargado = rs.getLong("ID_ENCARGADO");
			
			rest.add(new Restaurante(nombre,tipoComida,pagina,cuenta,encargado));
		}
		return rest;
	}
	
	/**
	 * 
	 */
	public void addRestaurante(Restaurante rest) throws SQLException, Exception {

		String sql = "INSERT INTO RESTAURANTE VALUES (";
		sql += rest.getId() + ",'";
		sql += rest.getPaginaWeb() + "','";
		sql += rest.getCuentaDePago() + "','";
		sql += rest.getNombre() + "',";
		sql += rest.getUsuariRestaurante() + ",";
		sql += rest.getTipoComida() + ")";

		PreparedStatement prepStmt = conn.prepareStatement(sql);
		recursos.add(prepStmt);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prepStmt.executeQuery();
		conn.commit();
	}
	
	public void deleteRestaurante (Restaurante tipo) throws SQLException
	{
		String sql = "DELETE FROM RESTAURANTE WHERE NIT = " + tipo.getId();
		
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prep.executeQuery();
		conn.commit();
	}
	
	public void surtir(Plato cambio, long nit) throws SQLException
	{
		//UPDATE PLATOS SET PLATOS.STOCK = cambio  WHERE NIT_RESTAURANTE = nit;
	
		String sql = "UPDATE PLATOS SET PLATOS.STOCK = " + cambio.getStock() + " WHERE NIT_RESTAURANTE  = " + nit;
		
		System.out.println(sql);
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
		prep.executeQuery();
		conn.commit();
	}
	
	/**
	 * @return 
	 * @throws SQLException 
	 * 
	 */
	public ArrayList<InfoVO> info() throws SQLException
	{
		//SELECT NIT_REST, SUM(CANTIDAD),SUM(PRECIO) FROM PEDIDO_PRODUCTO INNER JOIN PLATOS ON PEDIDO_PRODUCTO.ID_PRODUCTO = PLATOS.ID_PRODUCTO GROUP BY(NIT_REST);
		ArrayList<InfoVO> resp = new ArrayList<InfoVO>();
		String sql = "SELECT NIT_REST, SUM(CANTIDAD),SUM(PRECIO) FROM PEDIDO_PRODUCTO INNER JOIN PLATOS ON PEDIDO_PRODUCTO.ID_PRODUCTO = PLATOS.ID_PRODUCTO GROUP BY(NIT_REST)";
		
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		ResultSet rs = prep.executeQuery();
		conn.commit();
		
		while(rs.next())
		{
			long id = rs.getLong("NIT_REST");
			long cantidad = rs.getLong("SUM(CANTIDAD)");
			long precio = rs.getLong("SUM(PRECIO)");
			
			resp.add(new InfoVO(id,cantidad,precio));
		}
		return resp;
	}
	
	//iteracion 4
	
	/**
	 * metodo que devuelve la informacion sobre los clientes que pidieron en un restaurante dado entre un rango de fechas dado
	 */
	public ArrayList<UsuarioGeneral> darInfoClientes (long nit, String date1, String date2) throws SQLException
	{
		//SELECT NUM_DOCUMENTO, USER_NAME, TIPO_USUARIO, PASSWORD,NOMBRE, EMAIL FROM PEDIDO_PRODUCTO JOIN USUARIO ON PEDIDO_PRODUCTO.ID_CLIENTE = USUARIO.NUM_DOCUMENTO 
		//WHERE NIT_REST = 50 AND FECHA BETWEEN to_date('01/07/2006 1','DD/MM/YYYY HH') AND to_date('31/07/2006 12','DD/MM/YYYY HH'); 
		
		//alterar la sesion para que acepte las fechas que le pongo, podra cambiarse para la DB en general?
		String presql = "ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH'";
		System.out.println(presql);
		PreparedStatement preprep = conn.prepareStatement(presql);
		preprep.executeQuery();
		
		//ahora, la consulta;
		
		String sql = "SELECT NUM_DOCUMENTO, USER_NAME, TIPO_USUARIO, PASSWORD, NOMBRE, EMAIL FROM PEDIDO_PRODUCTO JOIN USUARIO ON PEDIDO_PRODUCTO.ID_CLIENTE = USUARIO.NUM_DOCUMENTO" +
		" WHERE NIT_REST = " + nit + " AND FECHA BETWEEN TO_DATE ('" + date1 + "','DD/MM/YYYY HH')" + " AND TO_DATE('" + date2 + "','DD/MM/YYYY HH')";
		
		System.out.println(sql); //debugging
		
		ArrayList<UsuarioGeneral> resp = new ArrayList<UsuarioGeneral> ();
		
		PreparedStatement prep = conn.prepareStatement(sql);
		recursos.add(prep);
		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		ResultSet rs = prep.executeQuery();
		
		while (rs.next())
		{
			String doc = rs.getString("NUM_DOCUMENTO");
			String username = rs.getString("USER_NAME");
			String pass = rs.getString("PASSWORD");
			String nombre = rs.getString("NOMBRE");
			String email = rs.getString("EMAIL");
			String rol = rs.getString("TIPO_USUARIO");
			
			resp.add(new UsuarioGeneral(username, pass, nombre, email, doc, darRol(rol) ) );
		}
		return resp;
		
	}
	
	public ArrayList<UsuarioGeneral> darInfoNoClientes(long nit, String date1, String date2) throws SQLException
	{
		//alterar la sesion para que acepte las fechas que le pongo, podra cambiarse para la DB en general?
				String presql = "ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH'";
				System.out.println(presql);
				PreparedStatement preprep = conn.prepareStatement(presql);
				preprep.executeQuery();
				
				//ahora, la consulta;
				
				String sql = "SELECT NUM_DOCUMENTO, USER_NAME, TIPO_USUARIO, PASSWORD, NOMBRE, EMAIL FROM PEDIDO_PRODUCTO JOIN USUARIO ON PEDIDO_PRODUCTO.ID_CLIENTE = USUARIO.NUM_DOCUMENTO" +
				" WHERE NIT_REST != " + nit + " AND FECHA BETWEEN TO_DATE ('" + date1 + "','DD/MM/YYYY HH')" + " AND TO_DATE('" + date2 + "','DD/MM/YYYY HH')";
				
				System.out.println(sql); //debugging
				
				ArrayList<UsuarioGeneral> resp = new ArrayList<UsuarioGeneral> ();
				
				PreparedStatement prep = conn.prepareStatement(sql);
				recursos.add(prep);
				conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
				ResultSet rs = prep.executeQuery();
				
				while (rs.next())
				{
					String doc = rs.getString("NUM_DOCUMENTO");
					String username = rs.getString("USER_NAME");
					String pass = rs.getString("PASSWORD");
					String nombre = rs.getString("NOMBRE");
					String email = rs.getString("EMAIL");
					String rol = rs.getString("TIPO_USUARIO");
					
					resp.add(new UsuarioGeneral(username, pass, nombre, email, doc, darRol(rol) ) );
	
				}
				return resp;
	}
	
	private RolUsuario darRol(String rol){
		if(rol.equals("UsuarioResturante")) return RolUsuario.UsuarioResturante;
		if(rol.equals("UsuarioCliente")) return RolUsuario.UsuarioCliente;
		if(rol.equals("UsuarioAdministrador")) return RolUsuario.UsuarioAdministrador;
		return null;
	}

}
