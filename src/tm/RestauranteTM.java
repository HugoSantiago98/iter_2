package tm;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import dao.DAOExitoRotonda;
import dao.DAORestaurante;
import dao.DAOTablaUsuario;
import dao.DAOZona;
import vos.ExitoRotondes;
import vos.InfoVO;
import vos.Plato;
import vos.Restaurante;
import vos.UsuarioGeneral;
import vos.Zona;



public class RestauranteTM {
	
	private static final String CONNECTION_DATA_FILE_NAME_REMOTE = "/conexion.properties";
	private  String connectionDataPath;
	private String user;
	private String password;
	private String url;
	private String driver;
	private Connection conn;

	public RestauranteTM(String contextPathP) {
		connectionDataPath = contextPathP + CONNECTION_DATA_FILE_NAME_REMOTE;
		initConnectionData();
	}

	private void initConnectionData() {
		try {
			File arch = new File(this.connectionDataPath);
			Properties prop = new Properties();
			FileInputStream in = new FileInputStream(arch);
			prop.load(in);
			in.close();
			this.url = prop.getProperty("url");
			this.user = prop.getProperty("usuario");
			this.password = prop.getProperty("clave");
			this.driver = prop.getProperty("driver");
			Class.forName(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private Connection darConexion() throws SQLException {
		System.out.println("Connecting to: " + url + " With user: " + user);
		return DriverManager.getConnection(url, user, password);
	}

	////////////////////////////////////////
	////////////////////////////////////////
	////////////////////////////////////////
	////////////////////////////////////////
	///////Transacciones////////////////////
	////////////////////////////////////////
	
	public List<Restaurante> darRestaurantes() throws Exception {
		List<Restaurante> list;
		DAORestaurante dao = new DAORestaurante();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			list = dao.darRestaurantes();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return list;
	}
	
	public void addRestaurante(Restaurante rest) throws Exception {
		DAORestaurante dao = new DAORestaurante();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			dao.addRestaurante(rest);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void deleteRestaurante(Restaurante tipo) throws Exception {
		DAORestaurante dao = new DAORestaurante();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			dao.deleteRestaurante(tipo);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	public void Surtir(Plato cambio, long nit) throws SQLException
	{
		DAORestaurante dao = new DAORestaurante();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			dao.surtir(cambio, nit);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public List<InfoVO> info () throws SQLException
	{
		List<InfoVO> list;
		DAORestaurante dao = new DAORestaurante();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			list = dao.info();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return list;
	}
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public List<ExitoRotondes> infoVentas() throws SQLException
	{
		List<ExitoRotondes> list;
		DAOExitoRotonda dao = new DAOExitoRotonda();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			list = dao.darExitosos();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarRecursos();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return list;
	}
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public List<UsuarioGeneral> infoClientes (long nit, String date1, String date2) throws SQLException
	{
		List<UsuarioGeneral> list;
		DAORestaurante dao = new DAORestaurante();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			list = dao.darInfoClientes(nit, date1, date2);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return list;
	}
	
	public List<UsuarioGeneral> infoNoClientes (long nit, String date1, String date2) throws SQLException
	{
		List<UsuarioGeneral> list;
		DAORestaurante dao = new DAORestaurante();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			list = dao.darInfoNoClientes(nit, date1, date2);

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
		return list;
	}
		
	
}
