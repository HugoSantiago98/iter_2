package tm;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import dao.DAOIngrediente;
import dao.DAOIngrediente_Equivalente;
import vos.Ingrediente;

public class Ingrediente_Equivalente {

	private static final String CONNECTION_DATA_FILE_NAME_REMOTE = "/conexion.properties";
	private  String connectionDataPath;
	private String user;
	private String password;
	private String url;
	private String driver;
	private Connection conn;

	public Ingrediente_Equivalente(String contextPathP) {
		connectionDataPath = contextPathP + CONNECTION_DATA_FILE_NAME_REMOTE;
		initConnectionData();
	}

	private void initConnectionData() {
		try {
			File arch = new File(this.connectionDataPath);
			Properties prop = new Properties();
			FileInputStream in = new FileInputStream(arch);
			prop.load(in);
			in.close();
			this.url = prop.getProperty("url");
			this.user = prop.getProperty("usuario");
			this.password = prop.getProperty("clave");
			this.driver = prop.getProperty("driver");
			Class.forName(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private Connection darConexion() throws SQLException {
		System.out.println("Connecting to: " + url + " With user: " + user);
		return DriverManager.getConnection(url, user, password);
	}
	
	public void addIngredienteEquivalente (vos.Ingrediente_Equivalente ingrediente) throws SQLException
	{
		DAOIngrediente_Equivalente dao = new DAOIngrediente_Equivalente();
		try 
		{
			//////transaccion
			this.conn = darConexion();
			dao.setConn(conn);
			dao.addIngredienteEquivalente(ingrediente);
			conn.commit();

		} catch (SQLException e) {
			System.err.println("SQLException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			System.err.println("GeneralException:" + e.getMessage());
			e.printStackTrace();
			throw e;
		} finally {
			try {
				dao.cerrarResources();
				if(this.conn!=null)
					this.conn.close();
			} catch (SQLException exception) {
				System.err.println("SQLException closing resources:" + exception.getMessage());
				exception.printStackTrace();
				throw exception;
			}
		}
	}
}
