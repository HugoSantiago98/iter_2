package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

import tm.UsuarioTM;
import tm.VideoAndesTM;
import vos.UsuarioCliente;
import vos.UsuarioGeneral;
import vos.Video;

@Path("usuarios")
public class UsuarioServices {

	@Context
	private ServletContext context;

	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}
	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUsuarios() {
		UsuarioTM tm = new UsuarioTM(getPath());
		List<UsuarioGeneral> usuarios;
		try {

			usuarios = tm.darUsuarios();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(usuarios).build();
	}

	@GET
	@Path( "{id: \\d+}" )
	@Produces( { MediaType.APPLICATION_JSON } )
	public Response getUsarioPorID( @PathParam( "id" ) Long id )
	{
		UsuarioTM tm = new UsuarioTM( getPath( ) );
		try
		{
			UsuarioGeneral v = tm.buscarUsuarioPorId( ""+id );
			return Response.status( 200 ).entity( v ).build( );			
		}
		catch( Exception e )
		{
			return Response.status( 500 ).entity( doErrorMessage( e ) ).build( );
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addUsuario(UsuarioGeneral usuario) {
		UsuarioTM tm = new UsuarioTM(getPath());
		try {
			tm.addUsuarios(usuario);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(usuario).build();
	}


	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUsuario(UsuarioGeneral usuarioGeneral) {
		UsuarioTM tm = new UsuarioTM(getPath());
		try {
			tm.deleteUsuario(usuarioGeneral);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(usuarioGeneral).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUsuario(UsuarioGeneral usuario) {
		UsuarioTM tm = new UsuarioTM(getPath());
		try {
			tm.updateUsuario(usuario);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(usuario).build();
	}

	//////////////////////////////////////////////////
	//Servicios relacionados con USUARIO CLIENTE
	//////////////////////////////////////////////////

	@POST
	@Path("/clientes")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response addUsuarioCliente(UsuarioCliente usuario) {
		System.out.println("Entra al addUsuarioCliente");
		UsuarioTM tm = new UsuarioTM(getPath());
		try {
			tm.addUsuariosCliente(usuario);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(usuario).build();
	}
}
