package rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tm.PedidoTM;
import tm.RestauranteTM;
import tm.ZonaTM;
import vos.Pedido;
import vos.Pedido_Producto;
import vos.Restaurante;
import vos.Zona;

@Path("pedidos")
public class PedidoService {

	@Context
	private ServletContext context;

	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}
	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPedido (Pedido_Producto pedido) {
		PedidoTM tm = new PedidoTM(getPath());
		try {
			tm.addPedido(pedido);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(pedido).build();
	}
	
	@POST
	@Path("/ConEquivalente")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPedidoEquivalente (Pedido_Producto pedido) {
		PedidoTM tm = new PedidoTM(getPath());
		try {
			tm.addPedidoEquivalencia(pedido);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(pedido).build();
	}
	
	@POST
	@Path("/List")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPedidoList(ArrayList<Pedido_Producto> pedido) {
		PedidoTM tm = new PedidoTM(getPath());
		try {
			tm.addPedidoEquivalenciaList(pedido);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(pedido).build();
	}
	
	@PUT
	@Path("/completo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response MarcarCompleto (Pedido_Producto pedido)
	{
		PedidoTM tm = new PedidoTM(getPath());
		try {
			tm.MarcarCompleto(pedido);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(pedido).build();
	}
	
	
	@PUT
	@Path("/ListCompleto")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response ListCompleto(ArrayList<Pedido_Producto> pedido)
	{
		PedidoTM tm = new PedidoTM(getPath());
		try {
			tm.ListCompleto(pedido);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(pedido).build();	
	}
	
	@PUT
	@Path("/cancelar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response cancelar (Pedido_Producto pedido)
	{
		PedidoTM tm = new PedidoTM(getPath());
		try {
			tm.CancelarList(pedido);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(pedido).build();
	}
	
	@GET
	@Path("/logueados")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response darLogueados()
	{
		PedidoTM tm = new PedidoTM(getPath());
		List<Pedido_Producto> rests;
		try {

			rests = tm.PedidosLogueados();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(rests).build();
	}
	
}