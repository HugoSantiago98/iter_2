package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tm.TipoComidaTM;
import tm.UsuarioTM;
import tm.VideoAndesTM;
import vos.TipoComida;
import vos.UsuarioGeneral;
import vos.Video;

@Path("tipocomida")
public class TipoComidaServices {

	@Context
	private ServletContext context;

	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}
	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getTipoComida() {
		TipoComidaTM tm = new TipoComidaTM(getPath());
		List<TipoComida> usuarios;
		try {

			usuarios = tm.darTipoComidas();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(usuarios).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addTipoComida(TipoComida tc) {
		TipoComidaTM tm = new TipoComidaTM(getPath());
		try {
			tm.addTipoComida(tc);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(tc).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteTipo(TipoComida tipo) {
		TipoComidaTM tm = new TipoComidaTM(getPath());
		try {
			tm.deleteTipoComida(tipo);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(tipo).build();
	}
	
}