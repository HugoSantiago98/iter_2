package rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tm.RestauranteTM;
import tm.ZonaPreferidaTM;
import vos.Restaurante;
import vos.ZonaPreferida;

@Path("zonaPreferida")
public class ZonaPreferidaServices {
	
	@Context
	private ServletContext context;

	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}
	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getZonaPreferida() {
		ZonaPreferidaTM tm = new ZonaPreferidaTM(getPath());
		List<ZonaPreferida> rests;
		try {

			rests = tm.darZonaPreferida();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(rests).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addZonaPreferida(ZonaPreferida rest) {
		ZonaPreferidaTM tm = new ZonaPreferidaTM(getPath());
		try {
			tm.addZonaPreferida(rest);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(rest).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteZonaPreferida(ZonaPreferida tipo) {
		ZonaPreferidaTM tm = new ZonaPreferidaTM(getPath());
		try {
			tm.deleteZonaPreferida(tipo);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(tipo).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateZonaPreferida(ZonaPreferida tipa)
	{
		ZonaPreferidaTM tm = new ZonaPreferidaTM(getPath());
		try {
			tm.updateZonaPreferida(tipa);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(tipa).build();
	}
	

}
