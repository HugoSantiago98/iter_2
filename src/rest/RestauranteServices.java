package rest;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import tm.RestauranteTM;
import tm.UsuarioTM;
import tm.ZonaTM;
import vos.ExitoRotondes;
import vos.InfoClientesINPUT;
import vos.InfoVO;
import vos.Plato;
import vos.Restaurante;
import vos.UsuarioGeneral;
import vos.Zona;

@Path("restaurante")
public class RestauranteServices {

	@Context
	private ServletContext context;

	private String getPath() {
		return context.getRealPath("WEB-INF/ConnectionData");
	}
	private String doErrorMessage(Exception e){
		return "{ \"ERROR\": \""+ e.getMessage() + "\"}" ;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getRestaurantes() {
		RestauranteTM tm = new RestauranteTM(getPath());
		List<Restaurante> rests;
		try {

			rests = tm.darRestaurantes();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(rests).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addRestaurante(Restaurante rest) {
		RestauranteTM tm = new RestauranteTM(getPath());
		try {
			tm.addRestaurante(rest);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(rest).build();
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRestaurante(Restaurante tipo) {
		RestauranteTM tm = new RestauranteTM(getPath());
		try {
			tm.deleteRestaurante(tipo);
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(tipo).build();
	}
	
	@PUT
	@Path( "{nit: \\d+}" )
	@Consumes( { MediaType.APPLICATION_JSON } )
	@Produces(MediaType.APPLICATION_JSON)
	public Response surtir( @PathParam( "nit" ) long nit , Plato cambio )
	{
		RestauranteTM tm = new RestauranteTM( getPath( ) );
		try
		{
			tm.Surtir(cambio, nit);
			return Response.status( 200 ).entity( cambio ).build( );			
		}
		catch( Exception e )
		{
			return Response.status( 500 ).entity( doErrorMessage( e ) ).build( );
		}
	}
	
	@GET
	@Path("/info")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response geInfo() {
		RestauranteTM tm = new RestauranteTM(getPath());
		List<InfoVO> rests;
		try {

			rests = tm.info();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(rests).build();
	}
	
	
	////////////////////////
	//Iteracion 4
	///////////////////////
	
	@GET
	@Path("/infoVentas")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getInfoVentas() {
		RestauranteTM tm = new RestauranteTM(getPath());
		List<ExitoRotondes> rests;
		try {

			rests = tm.infoVentas();
		} catch (Exception e) {
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(rests).build();
	}
	
	/**
	 * 
	 */
	@POST
	@Path("/infoClientes/{nit: \\d+}")
	@Consumes( { MediaType.APPLICATION_JSON } )
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfoClientes (@PathParam("nit") long nit, InfoClientesINPUT input)
	{
		RestauranteTM tm = new RestauranteTM(getPath());
		List<UsuarioGeneral> resp;
		
		try {
			System.out.println(input.getDate1());
			System.out.println(input.getDate2());
			resp = tm.infoClientes(nit, input.getDate1(), input.getDate2());
			System.out.println("maldito obama se cago mi DB :(");
		} catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(resp).build();
	}
	
	@POST
	@Path("/infoNoClientes/{nit: \\d+}")
	@Consumes( { MediaType.APPLICATION_JSON } )
	@Produces(MediaType.APPLICATION_JSON)
	public Response getInfoNoClientes (@PathParam("nit") long nit, InfoClientesINPUT input)
	{
		RestauranteTM tm = new RestauranteTM(getPath());
		List<UsuarioGeneral> resp;
		
		try {
			System.out.println(input.getDate1());
			System.out.println(input.getDate2());
			resp = tm.infoNoClientes(nit, input.getDate1(), input.getDate2());
			System.out.println("maldito obama se cago mi DB :(");
		} catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity(doErrorMessage(e)).build();
		}
		return Response.status(200).entity(resp).build();
	}
	
	
}